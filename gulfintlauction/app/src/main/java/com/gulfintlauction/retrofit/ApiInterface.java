package com.gulfintlauction.retrofit;


import com.gulfintlauction.models.AppConstant;
import com.gulfintlauction.models.BankModel;
import com.gulfintlauction.models.BidModel;
import com.gulfintlauction.models.BuyModel;
import com.gulfintlauction.models.CarLiveAuctionModel;
import com.gulfintlauction.models.ClaimRefundModel;
import com.gulfintlauction.models.CountModel;
import com.gulfintlauction.models.LedgerModel;
import com.gulfintlauction.models.LoginModel;
import com.gulfintlauction.models.PagesModel;
import com.gulfintlauction.models.ProfileModel;
import com.gulfintlauction.models.ProfileUpdateModel;
import com.gulfintlauction.models.WatchModel;


import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ApiInterface {

    @GET(AppConstant.URL_AUCTIONS)
    Call<ArrayList<CarLiveAuctionModel>> apiCarLiveAuctions();

    @GET(AppConstant.URL_AUCTIONS)
    Call<ArrayList<CarLiveAuctionModel>> apiCarLiveAuctions(@Query("id") int id);

    @GET(AppConstant.URL_AUCTIONS)
    Call<ArrayList<CarLiveAuctionModel>> apiCarLiveAuctions(@Query("customer_id") String customer_id);

    @GET(AppConstant.URL_AUCTIONS)
    Call<ArrayList<CarLiveAuctionModel>> apiCarLiveAuctions(@Query("id") int id, @Query("customer_id") String customer_id);

    @GET(AppConstant.URL_COMING_SOON)
    Call<ArrayList<CarLiveAuctionModel>> apiComingSoon();

    @GET(AppConstant.URL_COMING_SOON)
    Call<ArrayList<CarLiveAuctionModel>> apiComingSoon(@Query("id") int id);

    @GET(AppConstant.URL_AUCTIONS_count)
    Call<CountModel> apiCarLiveAuctionsCount();

    //    @GET(AppConstant.URL_LOGIN)
//    Call<LoginModel> apiLogin(@Query("username") String userName, @Query("password") String password);
    @GET(AppConstant.URL_LOGIN)
    Call<LoginModel> apiLogin(@Query("username") String userName, @Query("password") String password, @Query("fcm_token") String fcm_token);

    @GET(AppConstant.URL_UPDATE_FCM)
    Call<ResponseBody> apiUpdateToken(@Query("username") String userName, @Query("fcm_token") String fcm_token);

    @GET(AppConstant.URL_REGISTER)
    Call<LoginModel> apiRegister(@Query("first_name") String fname, @Query("last_name") String lname, @Query("username") String uname, @Query("password") String password, @Query("email") String email, @Query("contact_no") String contact);

    @GET(AppConstant.URL_BID_NOW)
    Call<BidModel> apiBidNow(@Query("customer_id") String customer_id, @Query("product_id") String product_id, @Query("bid_value") String bid_value, @Query("email_id") String email_id);

    @GET(AppConstant.URL_BUY_NOW)
    Call<BuyModel> apiBuyNow(@Query("customer_id") String customer_id, @Query("product_id") String product_id);

    @GET(AppConstant.URL_BUY_NOW)
    Call<BuyModel> apiBuyNow(@Query("customer_id") String customer_id, @Query("product_id") String product_id, @Query("status") String status);

    @GET(AppConstant.URL_PAGES)
    Call<PagesModel> apiPages(@Query("id") int id);

    @GET(AppConstant.URL_GET_PROFILE)
    Call<ProfileModel> apiGetProfile(@Query("customer_id") String id);

    @GET(AppConstant.URL_LEDGER)
    Call<ArrayList<LedgerModel>> apiGetLadger(@Query("customer_id") String id);

    @GET(AppConstant.URL_UPDATE_PROFILE)
    Call<ProfileUpdateModel> apiUpdateProfile(@Query("customer_id") String customer_id, @Query("title") String title, @Query("first_name") String first_name, @Query("last_name") String last_name, @Query("contact_no") String contact_no, @Query("date_of_birth") String date_of_birth, @Query("gender") String gender, @Query("nationality_id") String nationality_id, @Query("marital_status") String marital_status, @Query("password") String password, @Query("address_type") String address_type, @Query("address") String address, @Query("address_optional") String address_optional, @Query("city_town") String city_town, @Query("state_province") String state_province, @Query("post_code") String post_code, @Query("passport_no") String passport_no, @Query("issuing_country_id") String issuing_country_id, @Query("passport_file") String passport_file, @Query("passport_expiry_date") String passport_expiry_date, @Query("emirates_id") String emirates_id, @Query("emirates_file") String emirates_file, @Query("emirates_expiry_date") String emirates_expiry_date);

    @GET(AppConstant.URL_ADD_WATCH_LIST)
    Call<WatchModel> apiAddWatchList(@Query("customer_id") String customer_id, @Query("product_id") String product_id);

    @GET(AppConstant.URL_REMOVED_WATCH_LIST)
    Call<WatchModel> apiRemoveWatchList(@Query("customer_id") String customer_id, @Query("product_id") String product_id);


    @GET(AppConstant.URL_CLAIM_REFUND)
    Call<ClaimRefundModel> apiClaimRefund(@Query("customer_id") String customer_id, @Query("refund_amount") String refund_amount, @Query("refund_claim_by") String refund_claim_by, @Query("refund_claim_type") String refund_claim_type);

    @GET(AppConstant.URL_MY_WATCH_LIST)
    Call<ArrayList<CarLiveAuctionModel>> apiMyWatchList(@Query("customer_id") String customer_id);

    @GET(AppConstant.URL_ITEMS_YOU_BIDDING)
    Call<ArrayList<CarLiveAuctionModel>> apiItemsYoubidding(@Query("customer_id") String customer_id);

    @GET(AppConstant.URL_BIDS_YOU_WON)
    Call<ArrayList<CarLiveAuctionModel>> apiBidsYouWon(@Query("customer_id") String customer_id);

    @GET
    Call<ResponseBody> downloadFile(@Url String fileUrl);

    @GET(AppConstant.URL_MAKE_DEPOSIT)
    Call<BankModel> apimakeDeposit(@Query("deposit_amount") String deposit_amount, @Query("customer_id") String customer_id);


}
