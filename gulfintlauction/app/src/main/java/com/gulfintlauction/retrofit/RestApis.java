package com.gulfintlauction.retrofit;

import com.gulfintlauction.models.AppModel;
import com.gulfintlauction.models.CountModel;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by apple on 2/21/18.
 */

public class RestApis {
    private static final RestApis ourInstance = new RestApis();

    public static RestApis getInstance() {
        return ourInstance;
    }

    private RestApis() {
    }

    public void getCarsCount() {
        ApiClient.getClient().create(ApiInterface.class).apiCarLiveAuctionsCount().enqueue(new Callback<CountModel>() {

            @Override
            public void onResponse(Call<CountModel> call, Response<CountModel> response) {
                if (response.isSuccessful()) {
                    EventBus.getDefault().post(response.body());
                }
            }

            @Override
            public void onFailure(Call<CountModel> call, Throwable t) {
                EventBus.getDefault().post(null);
            }
        });
    }
}
