package com.gulfintlauction.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gulfintlauction.R;
import com.gulfintlauction.activities.CarDetailActivity;
import com.gulfintlauction.activities.CarsActivity;
import com.gulfintlauction.models.AppConstant;
import com.gulfintlauction.models.AppModel;
import com.gulfintlauction.models.BidModel;
import com.gulfintlauction.models.BuyModel;
import com.gulfintlauction.models.CarLiveAuctionModel;
import com.gulfintlauction.models.WatchModel;
import com.gulfintlauction.retrofit.ApiClient;
import com.gulfintlauction.retrofit.ApiInterface;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ammar on 28-Oct-17.
 */

public class CarsAuctionRecyclerViewAdapter extends RecyclerView.Adapter<CarsAuctionRecyclerViewAdapter.MyViewHolder> {


    ArrayList<CarLiveAuctionModel> data;
    Context context;
    int layout;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView makeModel;
        TextView menufectureDate;
        TextView lotNum;
        TextView bid;
        TextView noOfBid;
        TextView timer;
        TextView price;
        TextView bidNow;
        TextView buyNow;
        TextView posBid;
        TextView negBid;
        TextView valueBid;
        View rootView;
        TextView addorRemoveWatch;
        ImageView like;
        TextView wining_status;
        CountDownTimer mCountDownTimer;


        public MyViewHolder(View convertView) {
            super(convertView);
            rootView = convertView.findViewById(R.id.rootListView);
            makeModel = (TextView) convertView.findViewById(R.id.makemodel);
            menufectureDate = (TextView) convertView.findViewById(R.id.menufacture_date);
            lotNum = (TextView) convertView.findViewById(R.id.lot);
            bid = (TextView) convertView.findViewById(R.id.bid);
            noOfBid = (TextView) convertView.findViewById(R.id.no_of_bid);
            timer = (TextView) convertView.findViewById(R.id.timer);
            price = (TextView) convertView.findViewById(R.id.price);
            imageView = (ImageView) convertView.findViewById(R.id.imageView);
            bidNow = (TextView) convertView.findViewById(R.id.bidnow);
            buyNow = (TextView) convertView.findViewById(R.id.buynow);
            posBid = (TextView) convertView.findViewById(R.id.pos_bid);
            negBid = (TextView) convertView.findViewById(R.id.neg_bid);
            valueBid = (TextView) convertView.findViewById(R.id.value_bid);
            addorRemoveWatch = (TextView) convertView.findViewById(R.id.addWatch);
            like = (ImageView) convertView.findViewById(R.id.like);
            wining_status = (TextView) convertView.findViewById(R.id.wining_status);


        }
    }

    public void setData(ArrayList<CarLiveAuctionModel> data) {
        this.data = data;
    }


    public CarsAuctionRecyclerViewAdapter(ArrayList<CarLiveAuctionModel> data, Context context, int layout) {
        this.data = data;
        this.context = context;
        this.layout = layout;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        Picasso.with(context).load(data.get(position).getImages().get(0)).placeholder(R.drawable.image_not_found).into(holder.imageView);
        holder.makeModel.setText(data.get(position).getMake_name() + " / " + data.get(position).getModel_name());
        holder.menufectureDate.setText("Model : " + data.get(position).getYear_of_manufactured());
        holder.lotNum.setText("Car#" + data.get(position).getLot_no());
        holder.bid.setText("Bid : " + data.get(position).getLast_bid());
        holder.noOfBid.setText("No. Of Bids : " + data.get(position).getTotal_bids());
        holder.noOfBid.setTextColor(Color.parseColor("#c72127"));
        if (Integer.parseInt(data.get(position).getLast_bid().trim()) >= Integer.parseInt(data.get(position).getSale_type().trim())) {
            holder.buyNow.setVisibility(View.GONE);
        } else {
            holder.buyNow.setVisibility(View.VISIBLE);
        }
        if (AppModel.getInstance().readFromSharedPrefrence(context.getApplicationContext(), AppConstant.ISLOGIN).equals("true")) {
            holder.wining_status.setVisibility(View.VISIBLE);
            if (data.get(position).getWinning_status() != null) {
                if (data.get(position).getWinning_status().toLowerCase().contains("Winning")) {
                    holder.wining_status.setTextColor(Color.parseColor("#005200"));
                } else if (data.get(position).getWinning_status().toLowerCase().contains("out of bid")) {
                    holder.wining_status.setTextColor(Color.RED);

                }
                holder.wining_status.setText(data.get(position).getWinning_status());
            }
        } else {
            holder.wining_status.setVisibility(View.GONE);
        }

        if (holder.wining_status.getText().toString() != null) {
            if (holder.wining_status.getText().toString().equals("Winning")) {
                holder.wining_status.setTextColor(Color.parseColor("#005200"));
            } else {
                holder.wining_status.setTextColor(Color.RED);
            }
        }

        holder.valueBid.setText("" + data.get(position).getNext_bid_amount());
        long diff = Long.parseLong(data.get(position).getEnd_date_time()) - System.currentTimeMillis();
        if (holder.mCountDownTimer != null) {
            holder.mCountDownTimer.cancel();
        }
        holder.mCountDownTimer = new CountDownTimer(diff, 1000) {
            StringBuilder time = new StringBuilder();

            @Override
            public void onFinish() {
                holder.timer.setText(DateUtils.formatElapsedTime(0));
            }

            @Override
            public void onTick(long millisUntilFinished) {
                time.setLength(0);
                // Use days if appropriate
                if (millisUntilFinished > DateUtils.DAY_IN_MILLIS) {
                    long count = millisUntilFinished / DateUtils.DAY_IN_MILLIS;
                    if (count > 1)
                        time.append(count).append(" days ");
                    else
                        time.append(count).append(" day ");

                    millisUntilFinished %= DateUtils.DAY_IN_MILLIS;
                }

                time.append(DateUtils.formatElapsedTime(Math.round(millisUntilFinished / 1000d)));


                holder.timer.setText("End : " + time.toString());
                holder.timer.setTextColor(Color.parseColor("#005200"));

            }
        }.start();
        holder.price.setText("Buy Now : " + data.get(position).getSale_type() + " AED");


        holder.posBid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.valueBid.setText("" + (Integer.parseInt(holder.valueBid.getText().toString()) + 100));
            }
        });

        holder.negBid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int diff = (Integer.parseInt(holder.valueBid.getText().toString()) - 100);
                if (diff >= Integer.parseInt(data.get(position).getNext_bid_amount()))
                    holder.valueBid.setText("" + diff);
            }
        });

        holder.bidNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppModel.getInstance().readFromSharedPrefrence(context.getApplicationContext(), AppConstant.ISLOGIN).equals("true")) {
                    final ProgressDialog progressDialog = new ProgressDialog(context);
                    progressDialog.setMessage("Bidding in progress...");
                    progressDialog.show();
                    String cusID = AppModel.getInstance().readFromSharedPrefrence(context.getApplicationContext(), AppConstant.SHARED_PREF_CUSTOMER_ID);
                    String email = AppModel.getInstance().readFromSharedPrefrence(context.getApplicationContext(), AppConstant.SHARED_PREF_CUSTOMER_EMAIL);

                    String productID = data.get(position).getId();
                    String bidValue = holder.valueBid.getText().toString();

                    ApiClient.getClient().create(ApiInterface.class).apiBidNow(cusID, productID, bidValue, email).enqueue(new Callback<BidModel>() {
                        @Override
                        public void onResponse(Call<BidModel> call, Response<BidModel> response) {
                            if (response.isSuccessful() && response.body().isStatus()) {
                                data.get(position).setLast_bid("" + data.get(position).getNext_bid_amount());
                                data.get(position).setNext_bid_amount(response.body().getNext_bid_val());
                                int currentCount = Integer.parseInt(data.get(position).getTotal_bids().trim());
                                currentCount++;
                                data.get(position).setTotal_bids("" + currentCount);
                                notifyDataSetChanged();

                                Toast.makeText(context, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                            } else {
                                final AlertDialog.Builder dialog = new AlertDialog.Builder(context)
                                        .setMessage(response.body().getMessage())
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        });
                                dialog.show();
                            }
                            progressDialog.dismiss();

                        }

                        @Override
                        public void onFailure(Call<BidModel> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(context, "Network Failure, Please try again!", Toast.LENGTH_SHORT).show();
                        }
                    });

                } else {
                    Toast.makeText(context, "You've to Login first", Toast.LENGTH_SHORT).show();

                }
            }
        });

        holder.buyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppModel.getInstance().readFromSharedPrefrence(context.getApplicationContext(), AppConstant.ISLOGIN).equals("true")) {

                    final String cusID = AppModel.getInstance().readFromSharedPrefrence(context.getApplicationContext(), AppConstant.SHARED_PREF_CUSTOMER_ID);
                    final String productID = data.get(position).getId();

                    final ProgressDialog progressDialog = new ProgressDialog(context);
                    progressDialog.setMessage("Buying in progress...");
                    progressDialog.show();

                    ApiClient.getClient().create(ApiInterface.class).apiBuyNow(cusID, productID).enqueue(new Callback<BuyModel>() {
                        @Override
                        public void onResponse(Call<BuyModel> call, Response<BuyModel> response) {
                            progressDialog.dismiss();
                            if (response.isSuccessful()) {
                                if (response.body().getMessage().trim().toLowerCase().equals("Are You Sure".toLowerCase())) {
                                    final AlertDialog.Builder dialog = new AlertDialog.Builder(context)
                                            .setTitle("Are you sure, You want to buy ?")
                                            .setMessage("For " + data.get(position).getMake_name() + "/" + data.get(position).getModel_name() + " AED " + data.get(position).getSale_type() +
                                                    "\n including VAT amount AED " + response.body().getVat() +
                                                    "\n the above amount will be deducted from your balance")
                                            .setPositiveButton("Yes,Sure", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                    final ProgressDialog progressDialog = new ProgressDialog(context);
                                                    progressDialog.setMessage("Buying in progress...");
                                                    progressDialog.show();


                                                    ApiClient.getClient().create(ApiInterface.class).apiBuyNow(cusID, productID, "yes").enqueue(new Callback<BuyModel>() {
                                                        @Override
                                                        public void onResponse(Call<BuyModel> call, Response<BuyModel> response) {
                                                            if (response.isSuccessful()) {
                                                                data.remove(position);
                                                                notifyDataSetChanged();

                                                                Toast.makeText(context, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                                                            } else {
                                                                Toast.makeText(context, "Something went wrong, Please try again!", Toast.LENGTH_SHORT).show();
                                                            }
                                                            progressDialog.dismiss();
                                                        }

                                                        @Override
                                                        public void onFailure(Call<BuyModel> call, Throwable t) {
                                                            progressDialog.dismiss();
                                                            Toast.makeText(context, "Network Failure, Please try again!", Toast.LENGTH_SHORT).show();
                                                        }
                                                    });
                                                }
                                            })
                                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                }
                                            });
                                    dialog.show();


                                } else if (response.body().getMessage().trim().toLowerCase().equals("Low Balance".toLowerCase())) {

                                    final AlertDialog.Builder dialog = new AlertDialog.Builder(context)
                                            .setTitle("You have Insufficient Balance")
                                            .setMessage("For " + data.get(position).getMake_name() + "/" + data.get(position).getModel_name() + " AED " + data.get(position).getSale_type() +
                                                    "\n including VAT amount AED " + response.body().getVat())
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                }
                                            });
                                    dialog.show();

                                } else {
                                    Toast.makeText(context, "Something went wrong, Please try again!", Toast.LENGTH_SHORT).show();

                                }


                            } else {
                                Toast.makeText(context, "Something went wrong, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<BuyModel> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(context, "Network Failure, Please try again!", Toast.LENGTH_SHORT).show();
                        }
                    });


                } else {
                    Toast.makeText(context, "You've to Login first", Toast.LENGTH_SHORT).show();

                }
            }
        });

        holder.addorRemoveWatch.setText(data.get(position).getWatchlist_status());

        if (context instanceof CarsActivity) {
            holder.like.setVisibility(View.VISIBLE);

            if (data.get(position).getWatchlist_status().toLowerCase().equals("watching")) {
                holder.like.setColorFilter(Color.parseColor("#c72127"));
            } else if (data.get(position).getWatchlist_status().toLowerCase().equals("watch")) {
                holder.like.setColorFilter(Color.parseColor("#EEFFFFFF"));
            }
        } else {
            holder.like.setVisibility(View.GONE);
        }

        holder.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppModel.getInstance().readFromSharedPrefrence(context.getApplicationContext(), AppConstant.ISLOGIN).equals("true")) {
                    String cusID = AppModel.getInstance().readFromSharedPrefrence(context.getApplicationContext(), AppConstant.SHARED_PREF_CUSTOMER_ID);
                    String productID = data.get(position).getId();
                    if (holder.addorRemoveWatch.getText().toString().trim().toLowerCase().equals("watching")) {
                        final ProgressDialog removeDialog = new ProgressDialog(context);
                        removeDialog.setMessage("Removing from watchlist...");
                        removeDialog.show();

                        ApiClient.getClient().create(ApiInterface.class).apiRemoveWatchList(cusID, productID).enqueue(new Callback<WatchModel>() {
                            @Override
                            public void onResponse(Call<WatchModel> call, Response<WatchModel> response) {
                                if (response.isSuccessful()) {

                                    Toast.makeText(context, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    holder.addorRemoveWatch.setText("watch");
                                    holder.like.setColorFilter(Color.parseColor("#EEFFFFFF"));


                                } else {
                                    Toast.makeText(context, "Something went wrong, Please try again!", Toast.LENGTH_SHORT).show();
                                }

                                removeDialog.dismiss();

                            }

                            @Override
                            public void onFailure(Call<WatchModel> call, Throwable t) {
                                removeDialog.dismiss();
                                Toast.makeText(context, "Network Failure, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        final ProgressDialog addDialog = new ProgressDialog(context);
                        addDialog.setMessage("Adding to watchlist...");
                        addDialog.show();
                        ApiClient.getClient().create(ApiInterface.class).apiAddWatchList(cusID, productID).enqueue(new Callback<WatchModel>() {
                            @Override
                            public void onResponse(Call<WatchModel> call, Response<WatchModel> response) {
                                if (response.isSuccessful()) {

                                    Toast.makeText(context, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    holder.addorRemoveWatch.setText("watching");
                                    holder.like.setColorFilter(Color.parseColor("#c72127"));


                                } else {
                                    Toast.makeText(context, "Something went wrong, Please try again!", Toast.LENGTH_SHORT).show();
                                }

                                addDialog.dismiss();

                            }

                            @Override
                            public void onFailure(Call<WatchModel> call, Throwable t) {
                                addDialog.dismiss();
                                Toast.makeText(context, "Network Failure, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                } else {
                    Toast.makeText(context, "You've to Login first", Toast.LENGTH_SHORT).show();

                }
            }
        });


        holder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                //if (!(context instanceof ComingSoonActivity)) {
                Intent i = new Intent(context, CarDetailActivity.class);
                i.putExtra("id", Integer.parseInt(data.get(position).getId()));
                i.putExtra("lot", data.get(position).getLot_no());
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                context.startActivity(i);
                //}

            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
