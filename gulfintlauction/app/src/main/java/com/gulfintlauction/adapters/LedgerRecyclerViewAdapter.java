package com.gulfintlauction.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.barteksc.pdfviewer.PDFView;
import com.gulfintlauction.R;
import com.gulfintlauction.activities.PDFActivity;
import com.gulfintlauction.activities.PagesActivity;
import com.gulfintlauction.models.LedgerModel;

import java.util.ArrayList;

/**
 * Created by apple on 12/14/17.
 */

public class LedgerRecyclerViewAdapter extends RecyclerView.Adapter<LedgerRecyclerViewAdapter.MyViewHolder> {

    ArrayList<LedgerModel> data;
    Context context;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView dateTime;
        TextView discription;
        TextView amount;
        TextView balance;
        TextView viewPDF;


        public MyViewHolder(View convertView) {
            super(convertView);
            dateTime = (TextView) convertView.findViewById(R.id.date);
            discription = (TextView) convertView.findViewById(R.id.description);
            amount = (TextView) convertView.findViewById(R.id.amount);
            balance = (TextView) convertView.findViewById(R.id.balance);
            viewPDF = (TextView) convertView.findViewById(R.id.viewPDF);


        }
    }

    public LedgerRecyclerViewAdapter(ArrayList<LedgerModel> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public LedgerRecyclerViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ledger_list_new, parent, false);

        return new LedgerRecyclerViewAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.dateTime.setText(data.get(position).getDate_time());
        holder.discription.setText(data.get(position).getDescription() + "\nCar# " + data.get(position).getCar_no());
        if (data.get(position).getT_type().toLowerCase().equals("credit")) {
            holder.amount.setTextColor(Color.parseColor("#005200"));
        } else {
            holder.amount.setTextColor(Color.RED);
        }
        holder.amount.setText(data.get(position).getAmount());
        holder.balance.setText(data.get(position).getRemaining_balance());
        if (data.get(position).getTrans_pdf() != null) {
            if (data.get(position).getTrans_pdf().trim().equals("")) {
                holder.viewPDF.setVisibility(View.GONE);
            } else {
                holder.viewPDF.setVisibility(View.VISIBLE);
            }
        } else {
            holder.viewPDF.setVisibility(View.GONE);

        }
        holder.viewPDF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PDFActivity.class);


                intent.putExtra("link", data.get(position).getTrans_pdf().trim());

                intent.putExtra("title", data.get(position).getCar_no());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
