package com.gulfintlauction.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.gulfintlauction.R;
import com.gulfintlauction.helpers.App;
import com.gulfintlauction.models.AppModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Muhammad Waqas on 12/31/2017.
 */

public class ImageViewPagerAdapter extends PagerAdapter {

    private Activity _activity;
    private ArrayList<String> _imagePaths;
    private LayoutInflater inflater;

    // constructor
    public ImageViewPagerAdapter(Activity activity,
                                 ArrayList<String> imagePaths) {
        this._activity = activity;
        this._imagePaths = imagePaths;
    }

    @Override
    public int getCount() {
        return this._imagePaths.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imgDisplay;

        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.image_view_pager, container,
                false);

        imgDisplay = (ImageView) viewLayout.findViewById(R.id.images);
        Picasso.with(_activity).load(AppModel.getInstance().images.get(position)).into(imgDisplay);

        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ImageView) object);

    }
}