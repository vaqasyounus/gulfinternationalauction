package com.gulfintlauction.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gulfintlauction.R;
import com.gulfintlauction.activities.CarDetailActivity;
import com.gulfintlauction.activities.CarsActivity;
import com.gulfintlauction.activities.ComingSoonActivity;
import com.gulfintlauction.activities.ComingSoonDetailActivity;
import com.gulfintlauction.activities.PagesActivity;
import com.gulfintlauction.models.AppConstant;
import com.gulfintlauction.models.AppModel;
import com.gulfintlauction.models.BidModel;
import com.gulfintlauction.models.BuyModel;
import com.gulfintlauction.models.CarLiveAuctionModel;
import com.gulfintlauction.models.WatchModel;
import com.gulfintlauction.retrofit.ApiClient;
import com.gulfintlauction.retrofit.ApiInterface;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ammar on 28-Oct-17.
 */

public class ComingSoonRecyclerViewAdapter extends RecyclerView.Adapter<ComingSoonRecyclerViewAdapter.MyViewHolder> {


    ArrayList<CarLiveAuctionModel> data;
    Context context;
    int layout;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView title;
        TextView make;
        TextView model;
        TextView arrival_date;
        TextView menufacture_date;
        TextView mileage;
        TextView sendEnq;
        View rootView;


        public MyViewHolder(View convertView) {
            super(convertView);
            rootView = convertView.findViewById(R.id.rootListView);
            title = (TextView) convertView.findViewById(R.id.title);
            make = (TextView) convertView.findViewById(R.id.make);
            model = (TextView) convertView.findViewById(R.id.model);
            arrival_date = (TextView) convertView.findViewById(R.id.arrival_date);
            menufacture_date = (TextView) convertView.findViewById(R.id.menufacture_date);
            mileage = (TextView) convertView.findViewById(R.id.mileage);
            imageView = (ImageView) convertView.findViewById(R.id.imageView);
            sendEnq = (TextView) convertView.findViewById(R.id.inq);

        }
    }

    public void setData(ArrayList<CarLiveAuctionModel> data) {
        this.data = data;
    }


    public ComingSoonRecyclerViewAdapter(ArrayList<CarLiveAuctionModel> data, Context context, int layout) {
        this.data = data;
        this.context = context;
        this.layout = layout;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        Picasso.with(context).load(data.get(position).getImages().get(0)).placeholder(R.drawable.image_not_found).into(holder.imageView);
        holder.title.setText(data.get(position).getTitle_name());
        holder.make.setText("Make: " + data.get(position).getMake_name());
        holder.model.setText("Model : " + data.get(position).getModel_name());
        holder.arrival_date.setText("Arrival Date : "+data.get(position).getAdded_date());
        holder.menufacture_date.setText("Manufacture : "+data.get(position).getYear_of_manufactured());
        holder.mileage.setText("Mileage : "+data.get(position).getMileage());



        holder.sendEnq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PagesActivity.class);
                String url = "http://www.gulfintlauction.com/mobile/inquiry_mobile/"+data.get(position).getId();
                intent.putExtra("link", url);
                intent.putExtra("title", "Send Enquiry");
                context.startActivity(intent);
            }
        });



        holder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                    Intent i = new Intent(context, ComingSoonDetailActivity.class);
                    i.putExtra("id", Integer.parseInt(data.get(position).getId()));
                    i.putExtra("lot", data.get(position).getLot_no());
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    context.startActivity(i);

            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
