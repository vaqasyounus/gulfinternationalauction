package com.gulfintlauction.custom;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.gulfintlauction.R;

import java.io.IOException;

/**
 * Created by Ammar on 27-Oct-17.
 */

public class CustomDialog extends Dialog {

    public Activity c;
    ImageView animImageView;


    public CustomDialog(Activity a) {
        super(a);
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.custom_dialog);
        setCancelable(true);
        animImageView = (ImageView) findViewById(R.id.animImageView);
        try {
            Glide.with(c)
                    .load(R.raw.loader)
                    //.load("http://bestanimations.com/Nature/flowing-waterfall-cliff-green-nature-animated-gif.gif")
                    .into(animImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}