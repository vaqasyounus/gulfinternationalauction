package com.gulfintlauction.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.gulfintlauction.R;
import com.gulfintlauction.adapters.LedgerRecyclerViewAdapter;
import com.gulfintlauction.models.LedgerModel;

import java.util.ArrayList;

/**
 * Created by apple on 12/28/17.
 */


public class AllTransactionFragment extends Fragment {

    RecyclerView ledgerList;
    ArrayList<LedgerModel> data = new ArrayList<>();
    LedgerRecyclerViewAdapter ledgerRecyclerViewAdapter;
    RecyclerView.LayoutManager mLinearLayoutManager;
    LinearLayout loader_ll;


    public AllTransactionFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public AllTransactionFragment(ArrayList<LedgerModel> data) {
        this.data = data;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


//        this.loader_ll = (LinearLayout) findViewById(R.id.loader_ll);
//
//
//        ImageView animImageView = (ImageView) findViewById(R.id.animImageView);
//        loader_ll.setVisibility(View.VISIBLE);
//        try {
//            Glide.with(this)
//                    .load(R.raw.loader)
//                    .into(animImageView);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        View v = inflater.inflate(R.layout.all_transaction_fragment, container, false);
        ledgerRecyclerViewAdapter = new LedgerRecyclerViewAdapter(data, getActivity());
        mLinearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        ledgerList = (RecyclerView) v.findViewById(R.id.list_ledger);
        ledgerList.setLayoutManager(mLinearLayoutManager);
        ledgerList.setItemAnimator(new DefaultItemAnimator());
        ledgerList.setAdapter(ledgerRecyclerViewAdapter);


        // Inflate the layout for this fragment
        return v;
    }

}