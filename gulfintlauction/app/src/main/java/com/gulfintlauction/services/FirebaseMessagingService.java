package com.gulfintlauction.services;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;
import com.gulfintlauction.R;
import com.gulfintlauction.activities.DashboardActivity;
import com.squareup.picasso.Picasso;

import java.io.IOException;

/**
 * Created by LENOVO on 2/13/2018.
 */

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {
    private static final String TAG = "FCM Service";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        String title2 = remoteMessage.getData().get("title");
        String message2 = remoteMessage.getData().get("body");
        String ImageURL = remoteMessage.getData().get("image");
        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Intent intent = new Intent(this,DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);
        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setContentTitle(title2);
        mBuilder.setContentText(message2);
        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSound(uri);
        mBuilder.setSmallIcon(R.mipmap.app_icon);
        mBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);//must give priority to High, Max which will considered as heads-up notification

        try {
            mBuilder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(Picasso.with(getApplication()).load(ImageURL).get()));

            NotificationManager mNotifyMgr =
            (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            mNotifyMgr.notify(0, mBuilder.build());


            ;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
