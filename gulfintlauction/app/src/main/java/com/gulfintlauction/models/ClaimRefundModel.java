package com.gulfintlauction.models;

/**
 * Created by apple on 12/15/17.
 */

public class ClaimRefundModel {

    boolean status;
    String message;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
