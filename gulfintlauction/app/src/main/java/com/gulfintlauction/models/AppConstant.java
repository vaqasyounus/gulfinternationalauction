package com.gulfintlauction.models;

/**
 * Created by Muhammad Waqas on 2/23/2017.
 */

public class AppConstant {
    public static final String CONTENT_AUTHORITY = "com.dot.adhd.syncservice.datasync";
    public static final String CONTENT_PROVIDER_URI = "com.dot.adhd.syncservice.datasync.infoprovider";

    public static final String GULF_SHARED_PREF = "gulfsharedpref";
    public static final String SHARED_PREF_F_NAME = "customerfname";
    public static final String SHARED_PREF_L_NAME = "customerlname";
    public static final String SHARED_PREF_CUSTOMER_ID = "customerID";
    public static final String SHARED_PREF_CUSTOMER_EMAIL = "customerEmail";
    public static final String ISLOGIN = "islogin";
    public static final String USERNAME = "username";


    public static final String URL_AUCTIONS = "apis/auctions/auction";
    public static final String URL_LOGIN = "apis/auctions/login";
    public static final String URL_REGISTER = "apis/auctions/registration";
    public static final String URL_AUCTIONS_count = "apis/auctions/count_cars";
    public static final String URL_BID_NOW = "apis/auctions/bidnow";
    public static final String URL_BUY_NOW = "apis/auctions/buynow";
    public static final String URL_PAGES = "apis/auctions/pages";
    public static final String URL_GET_PROFILE = "apis/auctions/profile";
    public static final String URL_LEDGER = "apis/auctions/ledger";
    public static final String URL_UPDATE_PROFILE = "apis/auctions/profile_update";
    public static final String URL_ADD_WATCH_LIST = "apis/auctions/addtowatchlist";
    public static final String URL_REMOVED_WATCH_LIST = "apis/auctions/removetowatchlist";
    public static final String URL_COMING_SOON = "apis/auctions/comingsoon";
    public static final String URL_CLAIM_REFUND = "apis/auctions/claimrefund";
    public static final String URL_MY_WATCH_LIST = "apis/auctions/mywatchlist";
    public static final String URL_ITEMS_YOU_BIDDING = "apis/auctions/mybidinglist";
    public static final String URL_BIDS_YOU_WON = "apis/auctions/mywonlist";
    public static final String URL_MAKE_DEPOSIT = "apis/auctions/payment_gateway";
    public static final String URL_UPDATE_FCM = "apis/auction/update_fcm";
}
