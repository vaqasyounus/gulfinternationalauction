package com.gulfintlauction.models;

/**
 * Created by apple on 2/1/18.
 */

public class BankModel {
    boolean status;
    String bank_link;
    String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getBank_link() {
        return bank_link;
    }

    public void setBank_link(String bank_link) {
        this.bank_link = bank_link;
    }
}
