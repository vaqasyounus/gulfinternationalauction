package com.gulfintlauction.models;

/**
 * Created by MUHAMMAD WAQAS on 12/14/2017.
 */

public class LedgerModel {
    String id;
    String product_id;
    String invoice_no;
    String customer_id;
    String description;
    String invoice_type;
    String amount;
    String t_type;
    String mode_of_payment;
    String remaining_balance;
    String car_no;
    String trans_pdf;

    public String getRemaining_balance() {
        return remaining_balance;
    }

    public void setRemaining_balance(String remaining_balance) {
        this.remaining_balance = remaining_balance;
    }

    public String getCar_no() {
        return car_no;
    }

    public void setCar_no(String car_no) {
        this.car_no = car_no;
    }

    String date_time;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getInvoice_no() {
        return invoice_no;
    }

    public void setInvoice_no(String invoice_no) {
        this.invoice_no = invoice_no;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInvoice_type() {
        return invoice_type;
    }

    public void setInvoice_type(String invoice_type) {
        this.invoice_type = invoice_type;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getT_type() {
        return t_type;
    }

    public void setT_type(String t_type) {
        this.t_type = t_type;
    }

    public String getMode_of_payment() {
        return mode_of_payment;
    }

    public void setMode_of_payment(String mode_of_payment) {
        this.mode_of_payment = mode_of_payment;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getTrans_pdf() {
        return trans_pdf;
    }

    public void setTrans_pdf(String trans_pdf) {
        this.trans_pdf = trans_pdf;
    }
}
