package com.gulfintlauction.models;

/**
 * Created by Muhammad on 11/22/2017.
 */

public class BidModel {

    boolean status;
    String message;
    String next_bid_val;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNext_bid_val() {
        return next_bid_val;
    }

    public void setNext_bid_val(String next_bid_val) {
        this.next_bid_val = next_bid_val;
    }
}
