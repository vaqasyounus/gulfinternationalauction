package com.gulfintlauction.models;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Spinner;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Muhammad Waqas on 4/20/2017.
 */

public class AppModel {
    public Context currentActivity;
    public int countCars;
    public CarLiveAuctionModel data ;
    public ArrayList<String> images = new ArrayList<>();
    private static final AppModel ourInstance = new AppModel();

    public static AppModel getInstance() {
        return ourInstance;
    }

    private AppModel() {
    }

    public boolean isConnnected(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        return (networkInfo != null && networkInfo.isConnected());
    }


    public void writeToSharedPrefrence(Context context, String key, String value) {
        SharedPreferences sharedPref = context.getSharedPreferences(AppConstant.GULF_SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String readFromSharedPrefrence(Context context, String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(AppConstant.GULF_SHARED_PREF, Context.MODE_PRIVATE);
        return sharedPref.getString(key, "false");
    }

    public void removeSharedPrefrence(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(AppConstant.GULF_SHARED_PREF, Context.MODE_PRIVATE);
        sharedPref.edit().clear().commit();

    }

    public void hideKeyboard(ViewGroup viewGroup, Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(viewGroup.getWindowToken(), 0);
    }

    public boolean isValid(ViewGroup viewGroup) {
        for (View view : viewGroup.getTouchables()) {
            if (view instanceof EditText) {
                EditText editText = (EditText) view;
                String text = editText.getText().toString().trim();
                if (text.isEmpty() && !(editText.getHint().toString().equals("Child's Diagnosis Date") || editText.getHint().toString().equals("Child's Medication"))) {
                    editText.setError("Required");
                    editText.requestFocus();
                    return false;
                } else {
                    if (editText.getInputType() == InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS + InputType.TYPE_CLASS_TEXT) {
                        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(text).matches()) {
                            editText.setError("Invalid Email");
                            editText.requestFocus();
                            return false;
                        } else {
                            editText.setError(null);
                        }
                    } else {
                        editText.setError(null);
                    }
                }
            }
        }

        return true;
    }

    public Date timeStampToDate(long timeStamp) {
        return new Date(timeStamp);
    }

    public Date currentDate() {
        return new Date(System.currentTimeMillis());
    }

    public Date previous_n_days(int n) {
        return new Date(System.currentTimeMillis() - n * 24 * 3600 * 1000l); //Subtract n days
    }

    public long previous_n_days_milli_scond(int n) {
        long ms = (System.currentTimeMillis() - n * 24 * 3600 * 1000l);
        return ms;
    }

    public long dateToTimeStamp(String dateString) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = (Date) formatter.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    public void disable(ViewGroup viewGroup, boolean bool) {
        Iterator it = viewGroup.getTouchables().iterator();
        while (it.hasNext()) {
            View view = (View) it.next();
            if (view instanceof EditText) {
                EditText editText;
                if (bool) {
                    editText = (EditText) view;
                    editText.setFocusable(false);
                    editText.setCursorVisible(false);
                } else {
                    editText = (EditText) view;
                    editText.setCursorVisible(true);
                    editText.setEnabled(true);
                    editText.setFocusable(true);
                    editText.setFocusableInTouchMode(true);
                }
            } else if (view instanceof Spinner) {
//                Spinner spinner;
//                if (bool) {
//                    spinner = (Spinner)view;
//                    spinner.setEnabled(false);
//                } else {
//                    spinner = (Spinner)view;
//                    spinner.setEnabled(true);
//
//                }
            }
        }
    }

    public boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }


}
