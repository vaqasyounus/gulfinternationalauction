package com.gulfintlauction.models;

/**
 * Created by Muhammad on 11/22/2017.
 */

public class BuyModel {

    String message;
    String vat;
    String total;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;

    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
