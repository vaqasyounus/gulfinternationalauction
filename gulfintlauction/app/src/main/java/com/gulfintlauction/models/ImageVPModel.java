package com.gulfintlauction.models;

import java.util.ArrayList;

/**
 * Created by Muhammad Waqas on 12/31/2017.
 */

public class ImageVPModel {
    public ImageVPModel(ArrayList<String> imageList) {
        this.imageList = imageList;
    }

    public ArrayList<String> getImageList() {
        return imageList;
    }

    public void setImageList(ArrayList<String> imageList) {
        this.imageList = imageList;
    }

    ArrayList<String> imageList;
}
