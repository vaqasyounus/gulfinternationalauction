package com.gulfintlauction.models;

import com.gulfintlauction.helpers.App;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ammar on 23-Oct-17.
 */

public class CarLiveAuctionModel {
    String id;
    String lot_no;
    String title;
    String make_id;
    String model_id;
    String doc_type;
    String mileage;
    String features;
    String specification;
    String primary_damage;
    String secondary_damage;
    String chassis_no;
    String drive_type;
    String engine_type;
    String body_type_id;
    String color;
    String drive_type_two;
    String cylinders;
    String vehicle_key;
    String remarks;
    String sale_type;
    String min_bid_increment;
    String original_price;
    String current_price;
    String year_of_manufactured;
    String year_of_registration;
    String fuel_type_id;
    String notes;
    String location;
    //    String lat;
//    String long;
    String start_date_time;
    String end_date_time;
    String added_date;
    String arrival_date;
    String active;
    String auction_id;
    String auction_no;
    String won_customer_id;
    String won_customer_price;
    String user_views;
    String condition_report;
    String test_report;
    String make_name;
    String model_name;
    String title_name;

    public String getWinning_status() {
        return winning_status;
    }

    public void setWinning_status(String winning_status) {
        this.winning_status = winning_status;
    }

    String winning_status;

    public String getTitle_name() {
        return title_name;
    }

    public void setTitle_name(String title_name) {
        this.title_name = title_name;
    }

    String last_bid;
    String next_bid_amount;
    String total_bids;

    public String getTotal_bids() {
        return total_bids;
    }

    public void setTotal_bids(String total_bids) {
        this.total_bids = total_bids;
    }

    public String getWatchlist_status() {
        return watchlist_status;
    }

    public void setWatchlist_status(String watchlist_status) {
        this.watchlist_status = watchlist_status;
    }

    String watchlist_status;

    public String getNext_bid_amount() {
        return next_bid_amount;
    }

    public void setNext_bid_amount(String next_bid_amount) {
        this.next_bid_amount = next_bid_amount;
    }

    public String getLast_bid() {
        return last_bid;
    }

    public void setLast_bid(String last_bid) {
        this.last_bid = last_bid;
    }

    List<String> images;

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLot_no() {
        return lot_no;
    }

    public void setLot_no(String lot_no) {
        this.lot_no = lot_no;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMake_id() {
        return make_id;
    }

    public void setMake_id(String make_id) {
        this.make_id = make_id;
    }

    public String getModel_id() {
        return model_id;
    }

    public void setModel_id(String model_id) {
        this.model_id = model_id;
    }

    public String getDoc_type() {
        return doc_type;
    }

    public void setDoc_type(String doc_type) {
        this.doc_type = doc_type;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getFeatures() {
        return features;
    }

    public void setFeatures(String features) {
        this.features = features;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getPrimary_damage() {
        return primary_damage;
    }

    public void setPrimary_damage(String primary_damage) {
        this.primary_damage = primary_damage;
    }

    public String getSecondary_damage() {
        return secondary_damage;
    }

    public void setSecondary_damage(String secondary_damage) {
        this.secondary_damage = secondary_damage;
    }

    public String getChassis_no() {
        return chassis_no;
    }

    public void setChassis_no(String chassis_no) {
        this.chassis_no = chassis_no;
    }

    public String getDrive_type() {
        return drive_type;
    }

    public void setDrive_type(String drive_type) {
        this.drive_type = drive_type;
    }

    public String getEngine_type() {
        return engine_type;
    }

    public void setEngine_type(String engine_type) {
        this.engine_type = engine_type;
    }

    public String getBody_type_id() {
        return body_type_id;
    }

    public void setBody_type_id(String body_type_id) {
        this.body_type_id = body_type_id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getDrive_type_two() {
        return drive_type_two;
    }

    public void setDrive_type_two(String drive_type_two) {
        this.drive_type_two = drive_type_two;
    }

    public String getCylinders() {
        return cylinders;
    }

    public void setCylinders(String cylinders) {
        this.cylinders = cylinders;
    }

    public String getVehicle_key() {
        return vehicle_key;
    }

    public void setVehicle_key(String vehicle_key) {
        this.vehicle_key = vehicle_key;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSale_type() {
        return sale_type;
    }

    public void setSale_type(String sale_type) {
        this.sale_type = sale_type;
    }

    public String getMin_bid_increment() {
        return min_bid_increment;
    }

    public void setMin_bid_increment(String min_bid_increment) {
        this.min_bid_increment = min_bid_increment;
    }

    public String getOriginal_price() {
        return original_price;
    }

    public void setOriginal_price(String original_price) {
        this.original_price = original_price;
    }

    public String getCurrent_price() {
        return current_price;
    }

    public void setCurrent_price(String current_price) {
        this.current_price = current_price;
    }

    public String getYear_of_manufactured() {
        return year_of_manufactured;
    }

    public void setYear_of_manufactured(String year_of_manufactured) {
        this.year_of_manufactured = year_of_manufactured;
    }

    public String getYear_of_registration() {
        return year_of_registration;
    }

    public void setYear_of_registration(String year_of_registration) {
        this.year_of_registration = year_of_registration;
    }

    public String getFuel_type_id() {
        return fuel_type_id;
    }

    public void setFuel_type_id(String fuel_type_id) {
        this.fuel_type_id = fuel_type_id;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStart_date_time() {
        return ""+ AppModel.getInstance().dateToTimeStamp(start_date_time);
    }

    public void setStart_date_time(String start_date_time) {
        this.start_date_time = start_date_time;
    }

    public String getEnd_date_time() {
        return ""+AppModel.getInstance().dateToTimeStamp(end_date_time);
    }

    public void setEnd_date_time(String end_date_time) {
        this.end_date_time = end_date_time;
    }

    public String getAdded_date() {
        return added_date;
    }

    public void setAdded_date(String added_date) {
        this.added_date = added_date;
    }

    public String getArrival_date() {
        return arrival_date;
    }

    public void setArrival_date(String arrival_date) {
        this.arrival_date = arrival_date;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getAuction_id() {
        return auction_id;
    }

    public void setAuction_id(String auction_id) {
        this.auction_id = auction_id;
    }

    public String getAuction_no() {
        return auction_no;
    }

    public void setAuction_no(String auction_no) {
        this.auction_no = auction_no;
    }

    public String getWon_customer_id() {
        return won_customer_id;
    }

    public void setWon_customer_id(String won_customer_id) {
        this.won_customer_id = won_customer_id;
    }

    public String getWon_customer_price() {
        return won_customer_price;
    }

    public void setWon_customer_price(String won_customer_price) {
        this.won_customer_price = won_customer_price;
    }

    public String getUser_views() {
        return user_views;
    }

    public void setUser_views(String user_views) {
        this.user_views = user_views;
    }

    public String getCondition_report() {
        return condition_report;
    }

    public void setCondition_report(String condition_report) {
        this.condition_report = condition_report;
    }

    public String getTest_report() {
        return test_report;
    }

    public void setTest_report(String test_report) {
        this.test_report = test_report;
    }

    public String getMake_name() {
        return make_name;
    }

    public void setMake_name(String make_name) {
        this.make_name = make_name;
    }

    public String getModel_name() {
        return model_name;
    }

    public void setModel_name(String model_name) {
        this.model_name = model_name;
    }


}

