package com.gulfintlauction.models;

/**
 * Created by apple on 11/16/17.
 */

public class CountModel {

    int count_cars;

    public CountModel(int count_cars) {
        this.count_cars = count_cars;
    }

    public int getCount_cars() {
        return count_cars;
    }

    public void setCount_cars(int count_cars) {
        this.count_cars = count_cars;
    }
}
