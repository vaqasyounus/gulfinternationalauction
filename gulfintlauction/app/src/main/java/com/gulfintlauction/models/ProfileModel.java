package com.gulfintlauction.models;

/**
 * Created by MUHAMMAD WAQAS on 12/14/2017.
 */

public class ProfileModel {


    private Boolean status;
    private String customer_id;
    private String title;
    private String username;

    private String password;
    private String password_encrypted;
    private String marital_status;
    private String active;
    private String date_of_birth;
    private String gender;
    private String contact_no;
    private String address_type;
    private String address;
    private String address_optional;
    private String country_id;
    private String nationality_id;

    private String city_town;

    private String state_province;

    private String post_code;

    private String emirates_id_status;

    private String passport_status;

    private String passport_no;

    private String issuing_country_id;

    private String passport_file;

    private String passport_expiry_date;

    private String emirates_id;

    private String emirates_file;

    private String emirates_expiry_date;

    private String user_image;

    private String suspend;

    private String remote_ip;

    private String newsletter;

    private String banned;

    private String added_date;

    private String last_login;

    private String session_id;

    private String emirates_id_approved;

    private String passport_approved;

    private String email_id;

    private String first_name;

    private String last_name;


    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword_encrypted() {
        return password_encrypted;
    }

    public void setPassword_encrypted(String password_encrypted) {
        this.password_encrypted = password_encrypted;
    }

    public String getMarital_status() {
        return marital_status;
    }

    public void setMarital_status(String marital_status) {
        this.marital_status = marital_status;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getAddress_type() {
        return address_type;
    }

    public void setAddress_type(String address_type) {
        this.address_type = address_type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress_optional() {
        return address_optional;
    }

    public void setAddress_optional(String address_optional) {
        this.address_optional = address_optional;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getNationality_id() {
        return nationality_id;
    }

    public void setNationality_id(String nationality_id) {
        this.nationality_id = nationality_id;
    }

    public String getCity_town() {
        return city_town;
    }

    public void setCity_town(String city_town) {
        this.city_town = city_town;
    }

    public String getState_province() {
        return state_province;
    }

    public void setState_province(String state_province) {
        this.state_province = state_province;
    }

    public String getPost_code() {
        return post_code;
    }

    public void setPost_code(String post_code) {
        this.post_code = post_code;
    }

    public String getEmirates_id_status() {
        return emirates_id_status;
    }

    public void setEmirates_id_status(String emirates_id_status) {
        this.emirates_id_status = emirates_id_status;
    }

    public String getPassport_status() {
        return passport_status;
    }

    public void setPassport_status(String passport_status) {
        this.passport_status = passport_status;
    }

    public String getPassport_no() {
        return passport_no;
    }

    public void setPassport_no(String passport_no) {
        this.passport_no = passport_no;
    }

    public String getIssuing_country_id() {
        return issuing_country_id;
    }

    public void setIssuing_country_id(String issuing_country_id) {
        this.issuing_country_id = issuing_country_id;
    }

    public String getPassport_file() {
        return passport_file;
    }

    public void setPassport_file(String passport_file) {
        this.passport_file = passport_file;
    }

    public String getPassport_expiry_date() {
        return passport_expiry_date;
    }

    public void setPassport_expiry_date(String passport_expiry_date) {
        this.passport_expiry_date = passport_expiry_date;
    }

    public String getEmirates_id() {
        return emirates_id;
    }

    public void setEmirates_id(String emirates_id) {
        this.emirates_id = emirates_id;
    }

    public String getEmirates_file() {
        return emirates_file;
    }

    public void setEmirates_file(String emirates_file) {
        this.emirates_file = emirates_file;
    }

    public String getEmirates_expiry_date() {
        return emirates_expiry_date;
    }

    public void setEmirates_expiry_date(String emirates_expiry_date) {
        this.emirates_expiry_date = emirates_expiry_date;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getSuspend() {
        return suspend;
    }

    public void setSuspend(String suspend) {
        this.suspend = suspend;
    }

    public String getRemote_ip() {
        return remote_ip;
    }

    public void setRemote_ip(String remote_ip) {
        this.remote_ip = remote_ip;
    }

    public String getNewsletter() {
        return newsletter;
    }

    public void setNewsletter(String newsletter) {
        this.newsletter = newsletter;
    }

    public String getBanned() {
        return banned;
    }

    public void setBanned(String banned) {
        this.banned = banned;
    }

    public String getAdded_date() {
        return added_date;
    }

    public void setAdded_date(String added_date) {
        this.added_date = added_date;
    }

    public String getLast_login() {
        return last_login;
    }

    public void setLast_login(String last_login) {
        this.last_login = last_login;
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public String getEmirates_id_approved() {
        return emirates_id_approved;
    }

    public void setEmirates_id_approved(String emirates_id_approved) {
        this.emirates_id_approved = emirates_id_approved;
    }

    public String getPassport_approved() {
        return passport_approved;
    }

    public void setPassport_approved(String passport_approved) {
        this.passport_approved = passport_approved;
    }

    public String getEmail_id() {
        return email_id;
    }

    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }
}
