package com.gulfintlauction.Comparators;

import com.gulfintlauction.models.CarLiveAuctionModel;

import java.util.Comparator;

/**
 * Created by apple on 11/9/17.
 */

public class BidsComparatorAsc implements Comparator<CarLiveAuctionModel> {
    @Override
    public int compare(CarLiveAuctionModel t0, CarLiveAuctionModel t1) {
        int p0 = Integer.parseInt(t0.getLast_bid());
        int p1 = Integer.parseInt(t1.getLast_bid());

        if(p0==p1)
            return 0;
        else if(p0>p1)
            return 1;
        else
            return -1;
    }
}
