package com.gulfintlauction.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.gulfintlauction.R;
import com.gulfintlauction.models.AppModel;
import com.gulfintlauction.models.CarLiveAuctionModel;

public class SpecificationActivity extends AppCompatActivity {

    CarLiveAuctionModel data;
    TextView auction_id;
    TextView doctype;
    TextView mileage;
    TextView model;
    TextView primerydamage;
    TextView secondamage;
    TextView chasis;
    TextView drivertype;
    TextView enginetype;
    TextView bodystyle;
    TextView color;
    TextView wheeldrive;
    TextView cylender;
    TextView keys;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_specification);
        String title = getIntent().getStringExtra("title");
        getSupportActionBar().setTitle(title);
        data = AppModel.getInstance().data;

        auction_id = (TextView) findViewById(R.id.auctionid);
        doctype = (TextView) findViewById(R.id.doctype);
        model = (TextView) findViewById(R.id.model);
        mileage = (TextView) findViewById(R.id.mileage);
        primerydamage = (TextView) findViewById(R.id.primerydamage);
        secondamage = (TextView) findViewById(R.id.secondamage);
        chasis = (TextView) findViewById(R.id.chasis);
        drivertype = (TextView) findViewById(R.id.drivertype);
        enginetype = (TextView) findViewById(R.id.enginetype);
        bodystyle = (TextView) findViewById(R.id.bodystyle);
        color = (TextView) findViewById(R.id.color);
        wheeldrive = (TextView) findViewById(R.id.wheeldrive);
        cylender = (TextView) findViewById(R.id.cylender);
        keys = (TextView) findViewById(R.id.keys);


        auction_id.setText(data.getLot_no());
        doctype.setText(data.getDoc_type());
        model.setText(data.getYear_of_manufactured());
        mileage.setText(data.getMileage());
        primerydamage.setText(data.getPrimary_damage());
        secondamage.setText(data.getSecondary_damage());
        chasis.setText(data.getChassis_no());
        drivertype.setText(data.getDrive_type());
        enginetype.setText(data.getEngine_type());
        bodystyle.setText(data.getBody_type_id());
        color.setText(data.getColor());
        wheeldrive.setText(data.getDrive_type_two());
        cylender.setText(data.getCylinders());
        keys.setText(data.getVehicle_key());


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
