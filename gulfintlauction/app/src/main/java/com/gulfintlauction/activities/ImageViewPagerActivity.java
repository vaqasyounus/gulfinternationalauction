package com.gulfintlauction.activities;

import android.graphics.Path;
import android.icu.text.RelativeDateTimeFormatter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.gulfintlauction.R;
import com.gulfintlauction.adapters.ImageViewPagerAdapter;
import com.gulfintlauction.models.AppModel;

public class ImageViewPagerActivity extends AppCompatActivity {

    static TextView next;
    static TextView prev;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_image_view_pager);


        next = (TextView) findViewById(R.id.next);
        prev = (TextView) findViewById(R.id.prev);


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new ImageViewPagerAdapter(this, AppModel.getInstance().images));


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (viewPager.canScrollHorizontally(1)) {
//                    next.setVisibility(View.VISIBLE);
//                    prev.setVisibility(View.VISIBLE);
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
//                } else {
//                    next.setVisibility(View.GONE);
//                }

            }
        });

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (viewPager.canScrollHorizontally(-1)) {
//                    prev.setVisibility(View.VISIBLE);
//                    next.setVisibility(View.VISIBLE);
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
//                } else {
//                    prev.setVisibility(View.GONE);
//                }
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                if (position == 0) {
                    prev.setVisibility(View.GONE);
                    next.setVisibility(View.VISIBLE);
                } else if (position == AppModel.getInstance().images.size() - 1) {
                    prev.setVisibility(View.VISIBLE);
                    next.setVisibility(View.GONE);
                } else {
                    prev.setVisibility(View.VISIBLE);
                    next.setVisibility(View.VISIBLE);


                }
            }
        });
    }
}
