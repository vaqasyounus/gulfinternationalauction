package com.gulfintlauction.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.github.barteksc.pdfviewer.PDFView;
import com.gulfintlauction.R;
import com.gulfintlauction.retrofit.ApiClient;
import com.gulfintlauction.retrofit.ApiInterface;

import java.io.FileInputStream;
import java.io.InputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PDFActivity extends AppCompatActivity {
    PDFView pdfView;
    private ProgressBar progressBar;

    String TAG = "MYPDF";
    String url;
    String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);

        title = getIntent().getStringExtra("title");
        url = getIntent().getStringExtra("link");

        getSupportActionBar().setTitle(title);
        pdfView = (PDFView) findViewById(R.id.pdfView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);


        ApiClient.getClient().create(ApiInterface.class).downloadFile(url).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    Log.d(TAG, "server contacted and has file");
                    InputStream inputStream = response.body().byteStream();
                    pdfView.fromStream(inputStream).load();
                    pdfView.loadPages();



                } else {
                    Log.d(TAG, "server contact failed");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "error");
                progressBar.setVisibility(View.GONE);

            }
        });
    }
}
