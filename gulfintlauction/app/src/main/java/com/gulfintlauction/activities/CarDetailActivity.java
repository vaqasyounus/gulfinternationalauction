package com.gulfintlauction.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.gulfintlauction.R;
import com.gulfintlauction.models.AppConstant;
import com.gulfintlauction.models.AppModel;
import com.gulfintlauction.models.BidModel;
import com.gulfintlauction.models.BuyModel;
import com.gulfintlauction.models.CarLiveAuctionModel;
import com.gulfintlauction.models.WatchModel;
import com.gulfintlauction.retrofit.ApiClient;
import com.gulfintlauction.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarDetailActivity extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    private SliderLayout mDemoSlider;
    LinearLayout loader_ll;
    //    LinearLayout anOtherView;
    Toolbar toolbar;
    TextView cur_price;
    TextView min_inc;
    TextView last_bid;
    TextView time_remaining;
    CountDownTimer mCountDownTimer;
    TextView no_of_bids;
    int id;
    Button watch;
    Button share;
    TextView posBid;
    TextView negBid;
    TextView valueBid;
    TextView bidNow;
    TextView buyNow;
    TextView specification;
    TextView map;
    TextView details;


    CarLiveAuctionModel carLiveAuctionModel;

    @Override
    protected void onResume() {
        super.onResume();
        if (AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.ISLOGIN).equals("true")) {
//            anOtherView.setVisibility(View.GONE);
        } else {
//            anOtherView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_detail);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.cars));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("Car # " + getIntent().getStringExtra("lot"));
        AppModel.getInstance().images = new ArrayList<>();


        mDemoSlider = (SliderLayout) findViewById(R.id.slider);
        watch = (Button) findViewById(R.id.watch);
        share = (Button) findViewById(R.id.share);
        cur_price = (TextView) findViewById(R.id.current_price);
        min_inc = (TextView) findViewById(R.id.min_inc);
        last_bid = (TextView) findViewById(R.id.last_bid);
        time_remaining = (TextView) findViewById(R.id.time_remaining);
        no_of_bids = (TextView) findViewById(R.id.no_of_bids);
        posBid = (TextView) findViewById(R.id.pos_bid);
        negBid = (TextView) findViewById(R.id.neg_bid);
        bidNow = (TextView) findViewById(R.id.bidnow);
        buyNow = (TextView) findViewById(R.id.buynow);
        specification = (TextView) findViewById(R.id.specification);
        map = (TextView) findViewById(R.id.map);
        valueBid = (TextView) findViewById(R.id.value_bid);
        details = (TextView) findViewById(R.id.details);

        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CarDetailActivity.this, PagesActivity.class);
                intent.putExtra("link", "http://www.gulfintlauction.com/map_mobile/" + carLiveAuctionModel.getId());
                intent.putExtra("title", getString(R.string.location));
                startActivity(intent);
            }
        });

        specification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppModel.getInstance().data = carLiveAuctionModel;
                Intent intent = new Intent(CarDetailActivity.this, SpecificationActivity.class);
                intent.putExtra("title", carLiveAuctionModel.getLot_no());
                startActivity(intent);
            }
        });

        posBid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                valueBid.setText("" + (Integer.parseInt(valueBid.getText().toString()) + 100));
            }
        });

        negBid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int diff = (Integer.parseInt(valueBid.getText().toString()) - 100);
                if (diff >= Integer.parseInt(carLiveAuctionModel.getNext_bid_amount()))
                    valueBid.setText("" + diff);
            }
        });
        bidNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.ISLOGIN).equals("true")) {

                    String cusID = AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.SHARED_PREF_CUSTOMER_ID);
                    String email = AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.SHARED_PREF_CUSTOMER_EMAIL);

                    String productID = carLiveAuctionModel.getId();
                    String bidValue = valueBid.getText().toString();

                    ApiClient.getClient().create(ApiInterface.class).apiBidNow(cusID, productID, bidValue, email).enqueue(new Callback<BidModel>() {
                        @Override
                        public void onResponse(Call<BidModel> call, Response<BidModel> response) {
                            if (response.isSuccessful() && response.body().isStatus()) {
                                carLiveAuctionModel.setLast_bid("" + carLiveAuctionModel.getNext_bid_amount());
                                carLiveAuctionModel.setNext_bid_amount(response.body().getNext_bid_val());
                                valueBid.setText("" + carLiveAuctionModel.getNext_bid_amount());
                                last_bid.setText("" + carLiveAuctionModel.getLast_bid());

                                int currentCount = Integer.parseInt(no_of_bids.getText().toString().trim());
                                currentCount++;
                                no_of_bids.setText("  " + currentCount);
                                if (Integer.parseInt(carLiveAuctionModel.getLast_bid().trim()) >= Integer.parseInt(carLiveAuctionModel.getSale_type().trim())) {
                                    buyNow.setVisibility(View.GONE);
                                } else {
                                    buyNow.setVisibility(View.VISIBLE);
                                }
                                Toast.makeText(CarDetailActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                            } else {
                                final AlertDialog.Builder dialog = new AlertDialog.Builder(CarDetailActivity.this)
                                        .setMessage(response.body().getMessage())
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        });
                                dialog.show();
                            }

                        }

                        @Override
                        public void onFailure(Call<BidModel> call, Throwable t) {
                            Toast.makeText(CarDetailActivity.this, "Network Failure, Please try again!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    Toast.makeText(CarDetailActivity.this, "You've to Login first", Toast.LENGTH_SHORT).show();

                }
            }
        });

        buyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppModel.getInstance().readFromSharedPrefrence(CarDetailActivity.this, AppConstant.ISLOGIN).equals("true")) {

                    final String cusID = AppModel.getInstance().readFromSharedPrefrence(CarDetailActivity.this, AppConstant.SHARED_PREF_CUSTOMER_ID);
                    final String productID = carLiveAuctionModel.getId();

                    final ProgressDialog progressDialog = new ProgressDialog(CarDetailActivity.this);
                    progressDialog.setMessage("Buying in progress...");
                    progressDialog.show();

                    ApiClient.getClient().create(ApiInterface.class).apiBuyNow(cusID, productID).enqueue(new Callback<BuyModel>() {
                        @Override
                        public void onResponse(Call<BuyModel> call, Response<BuyModel> response) {
                            progressDialog.dismiss();
                            if (response.isSuccessful()) {
                                if (response.body().getMessage().trim().toLowerCase().equals("Are You Sure".toLowerCase())) {
                                    final AlertDialog.Builder dialog = new AlertDialog.Builder(CarDetailActivity.this)
                                            .setTitle("Are you sure, You want to buy ?")
                                            .setMessage("For " + carLiveAuctionModel.getMake_name() + "/" + carLiveAuctionModel.getModel_name() + " AED " + carLiveAuctionModel.getSale_type() +
                                                    "\n including VAT amount AED " + response.body().getVat() +
                                                    "\n the above amount will be deducted from your balance")
                                            .setPositiveButton("Yes,Sure", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                    final ProgressDialog progressDialog = new ProgressDialog(CarDetailActivity.this);
                                                    progressDialog.setMessage("Buying in progress...");
                                                    progressDialog.show();


                                                    ApiClient.getClient().create(ApiInterface.class).apiBuyNow(cusID, productID, "yes").enqueue(new Callback<BuyModel>() {
                                                        @Override
                                                        public void onResponse(Call<BuyModel> call, Response<BuyModel> response) {
                                                            if (response.isSuccessful()) {

                                                                Toast.makeText(CarDetailActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                                                            } else {
                                                                Toast.makeText(CarDetailActivity.this, "Something went wrong, Please try again!", Toast.LENGTH_SHORT).show();
                                                            }
                                                            progressDialog.dismiss();
                                                        }

                                                        @Override
                                                        public void onFailure(Call<BuyModel> call, Throwable t) {
                                                            progressDialog.dismiss();
                                                            Toast.makeText(CarDetailActivity.this, "Network Failure, Please try again!", Toast.LENGTH_SHORT).show();
                                                        }
                                                    });
                                                }
                                            })
                                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                }
                                            });
                                    dialog.show();


                                } else if (response.body().getMessage().trim().toLowerCase().equals("Low Balance".toLowerCase())) {

                                    final AlertDialog.Builder dialog = new AlertDialog.Builder(CarDetailActivity.this)
                                            .setTitle("You have Insufficient Balance")
                                            .setMessage("For " + carLiveAuctionModel.getMake_name() + "/" + carLiveAuctionModel.getModel_name() + " AED " + carLiveAuctionModel.getSale_type() +
                                                    "\n including VAT amount AED " + response.body().getVat())
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                }
                                            });
                                    dialog.show();

                                } else {
                                    Toast.makeText(CarDetailActivity.this, "Something went wrong, Please try again!", Toast.LENGTH_SHORT).show();

                                }


                            } else {
                                Toast.makeText(CarDetailActivity.this, "Something went wrong, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<BuyModel> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(CarDetailActivity.this, "Network Failure, Please try again!", Toast.LENGTH_SHORT).show();
                        }
                    });


                } else {
                    Toast.makeText(CarDetailActivity.this, "You've to Login first", Toast.LENGTH_SHORT).show();

                }
            }
        });


//        anOtherView = (LinearLayout) findViewById(R.id.anotherView);

        this.loader_ll = (LinearLayout) findViewById(R.id.loader_ll);
        final ImageView animImageView = (ImageView) findViewById(R.id.animImageView);
        loader_ll.setVisibility(View.VISIBLE);
        try {
            Glide.with(this)
                    .load(R.raw.loader)
                    .into(animImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        id = getIntent().getIntExtra("id", 0);
        String cusID = AppModel.getInstance().readFromSharedPrefrence(CarDetailActivity.this, AppConstant.SHARED_PREF_CUSTOMER_ID);
        final HashMap<String, String> url_maps = new HashMap<String, String>();

        if (AppModel.getInstance().readFromSharedPrefrence(CarDetailActivity.this, AppConstant.ISLOGIN).equals("true")) {

            ApiClient.getClient().create(ApiInterface.class).apiCarLiveAuctions(id, cusID).enqueue(new Callback<ArrayList<CarLiveAuctionModel>>() {

                @Override
                public void onResponse(Call<ArrayList<CarLiveAuctionModel>> call, Response<ArrayList<CarLiveAuctionModel>> response) {
                    ArrayList<CarLiveAuctionModel> carLiveAuctionModels = response.body();
                    Log.d("waqas", response.body().toString());
                    carLiveAuctionModel = carLiveAuctionModels.get(0);
                    valueBid.setText(carLiveAuctionModel.getNext_bid_amount());
                    cur_price.setText("AED " + carLiveAuctionModel.getCurrent_price());
                    min_inc.setText("AED " + carLiveAuctionModel.getMin_bid_increment());
                    last_bid.setText("AED " + carLiveAuctionModel.getLast_bid());
                    no_of_bids.setText(" " + carLiveAuctionModel.getTotal_bids());
                    details.setText("Remarks : " + carLiveAuctionModel.getRemarks());
                    watch.setText(carLiveAuctionModel.getWatchlist_status().trim().toLowerCase().equals("watch") ? getString(R.string.watch) : getString(R.string.watching));
                    if (Integer.parseInt(carLiveAuctionModel.getLast_bid().trim()) >= Integer.parseInt(carLiveAuctionModel.getSale_type().trim())) {
                        buyNow.setVisibility(View.GONE);
                    } else {
                        buyNow.setVisibility(View.VISIBLE);
                    }
                    long diff = Long.parseLong(carLiveAuctionModel.getEnd_date_time()) - System.currentTimeMillis();
                    mCountDownTimer = new CountDownTimer(diff, 1000) {
                        StringBuilder time = new StringBuilder();

                        @Override
                        public void onFinish() {
                            time_remaining.setText(DateUtils.formatElapsedTime(0));
                        }

                        @Override
                        public void onTick(long millisUntilFinished) {
                            time.setLength(0);
                            if (millisUntilFinished > DateUtils.DAY_IN_MILLIS) {
                                long count = millisUntilFinished / DateUtils.DAY_IN_MILLIS;
                                if (count > 1)
                                    time.append(count).append(" days ");
                                else
                                    time.append(count).append(" day ");
                                millisUntilFinished %= DateUtils.DAY_IN_MILLIS;
                            }
                            time.append(DateUtils.formatElapsedTime(Math.round(millisUntilFinished / 1000d)));
                            time_remaining.setText(" " + time.toString());

                        }
                    }.start();


                    List<String> images = carLiveAuctionModels.get(0).getImages();
                    for (int i = 0; i < images.size(); i++) {
                        AppModel.getInstance().images.add(images.get(i));
                        url_maps.put(carLiveAuctionModels.get(0).getMake_name() + " " + (i + 1), images.get(i));
                    }
                    loader_ll.setVisibility(View.GONE);
                    for (String name : url_maps.keySet()) {
                        TextSliderView textSliderView = new TextSliderView(CarDetailActivity.this);
                        // initialize a SliderLayout
                        textSliderView
                                .description(name)
                                .image(url_maps.get(name))
                                .setScaleType(BaseSliderView.ScaleType.Fit)
                                .description(carLiveAuctionModels.get(0).getMake_name())
                                .setOnSliderClickListener(CarDetailActivity.this);


                        //add your extra information
                        textSliderView.bundle(new Bundle());
                        textSliderView.getBundle()
                                .putString("extra", name);

                        mDemoSlider.addSlider(textSliderView);
                    }
                    mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
                    mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                    mDemoSlider.setCustomAnimation(new DescriptionAnimation());
                    mDemoSlider.setDuration(5000);
                    mDemoSlider.addOnPageChangeListener(CarDetailActivity.this);
                }

                @Override
                public void onFailure(Call<ArrayList<CarLiveAuctionModel>> call, Throwable t) {
                    loader_ll.setVisibility(View.GONE);

                }
            });

        } else {
            ApiClient.getClient().create(ApiInterface.class).apiCarLiveAuctions(id).enqueue(new Callback<ArrayList<CarLiveAuctionModel>>() {

                @Override
                public void onResponse(Call<ArrayList<CarLiveAuctionModel>> call, Response<ArrayList<CarLiveAuctionModel>> response) {
                    ArrayList<CarLiveAuctionModel> carLiveAuctionModels = response.body();
                    carLiveAuctionModel = carLiveAuctionModels.get(0);
                    valueBid.setText(carLiveAuctionModel.getNext_bid_amount());
                    cur_price.setText("AED " + carLiveAuctionModel.getCurrent_price());
                    min_inc.setText("AED " + carLiveAuctionModel.getMin_bid_increment());
                    last_bid.setText("AED " + carLiveAuctionModel.getLast_bid());
                    no_of_bids.setText(" " + carLiveAuctionModel.getTotal_bids());
                    watch.setText(carLiveAuctionModel.getWatchlist_status().trim().toLowerCase().equals("watch") ? getString(R.string.watch) : getString(R.string.watching));
                    if (Integer.parseInt(carLiveAuctionModel.getLast_bid().trim()) >= Integer.parseInt(carLiveAuctionModel.getSale_type().trim())) {
                        buyNow.setVisibility(View.GONE);
                    } else {
                        buyNow.setVisibility(View.VISIBLE);
                    }
                    long diff = Long.parseLong(carLiveAuctionModel.getEnd_date_time()) - System.currentTimeMillis();
                    mCountDownTimer = new CountDownTimer(diff, 1000) {
                        StringBuilder time = new StringBuilder();

                        @Override
                        public void onFinish() {
                            time_remaining.setText(DateUtils.formatElapsedTime(0));
                        }

                        @Override
                        public void onTick(long millisUntilFinished) {
                            time.setLength(0);
                            if (millisUntilFinished > DateUtils.DAY_IN_MILLIS) {
                                long count = millisUntilFinished / DateUtils.DAY_IN_MILLIS;
                                if (count > 1)
                                    time.append(count).append(" days ");
                                else
                                    time.append(count).append(" day ");
                                millisUntilFinished %= DateUtils.DAY_IN_MILLIS;
                            }
                            time.append(DateUtils.formatElapsedTime(Math.round(millisUntilFinished / 1000d)));
                            time_remaining.setText(" " + time.toString());

                        }
                    }.start();


                    List<String> images = carLiveAuctionModels.get(0).getImages();
                    for (int i = 0; i < images.size(); i++) {
                        AppModel.getInstance().images.add(images.get(i));
                        url_maps.put(carLiveAuctionModels.get(0).getModel_name() + " " + (i + 1), images.get(i));
                    }
                    loader_ll.setVisibility(View.GONE);
                    for (String name : url_maps.keySet()) {
                        TextSliderView textSliderView = new TextSliderView(CarDetailActivity.this);
                        // initialize a SliderLayout
                        textSliderView
                                .description(name)
                                .image(url_maps.get(name))
                                .setScaleType(BaseSliderView.ScaleType.CenterInside)
                                .setOnSliderClickListener(CarDetailActivity.this);


                        //add your extra information
                        textSliderView.bundle(new Bundle());
                        textSliderView.getBundle()
                                .putString("extra", name);

                        mDemoSlider.addSlider(textSliderView);
                    }
                    mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
                    mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                    mDemoSlider.setCustomAnimation(new DescriptionAnimation());
                    mDemoSlider.setDuration(5000);
                    mDemoSlider.addOnPageChangeListener(CarDetailActivity.this);
                }

                @Override
                public void onFailure(Call<ArrayList<CarLiveAuctionModel>> call, Throwable t) {
                    loader_ll.setVisibility(View.GONE);

                }
            });

        }

        watch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppModel.getInstance().readFromSharedPrefrence(CarDetailActivity.this, AppConstant.ISLOGIN).equals("true")) {

                    String cusID = AppModel.getInstance().readFromSharedPrefrence(CarDetailActivity.this, AppConstant.SHARED_PREF_CUSTOMER_ID);
                    String productID = carLiveAuctionModel.getId();
                    if (watch.getText().toString().trim().toLowerCase().equals(getString(R.string.watching))) {
                        ApiClient.getClient().create(ApiInterface.class).apiRemoveWatchList(cusID, productID).enqueue(new Callback<WatchModel>() {
                            @Override
                            public void onResponse(Call<WatchModel> call, Response<WatchModel> response) {
                                if (response.isSuccessful()) {

                                    Toast.makeText(CarDetailActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    watch.setText(getString(R.string.watch));


                                } else {
                                    Toast.makeText(CarDetailActivity.this, "Something went wrong, Please try again!", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(Call<WatchModel> call, Throwable t) {
                                Toast.makeText(CarDetailActivity.this, "Network Failure, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        ApiClient.getClient().create(ApiInterface.class).apiAddWatchList(cusID, productID).enqueue(new Callback<WatchModel>() {
                            @Override
                            public void onResponse(Call<WatchModel> call, Response<WatchModel> response) {
                                if (response.isSuccessful()) {

                                    Toast.makeText(CarDetailActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    watch.setText(getString(R.string.watching));


                                } else {
                                    Toast.makeText(CarDetailActivity.this, "Something went wrong, Please try again!", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(Call<WatchModel> call, Throwable t) {
                                Toast.makeText(CarDetailActivity.this, "Network Failure, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                } else {
                    Toast.makeText(CarDetailActivity.this, "You've to Login first", Toast.LENGTH_SHORT).show();

                }
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                share.putExtra(Intent.EXTRA_SUBJECT, "Gulf International Auction");
                share.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.gulfintlauction");
                startActivity(Intent.createChooser(share, "Gulf International Auction"));
            }
        });


    }

    @Override
    protected void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        mDemoSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        startActivity(new Intent(CarDetailActivity.this, ImageViewPagerActivity.class));
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }
}