package com.gulfintlauction.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gulfintlauction.Comparators.BidsComparatorAsc;
import com.gulfintlauction.Comparators.BidsComparatorDesc;
import com.gulfintlauction.Comparators.EndingComparatorAsc;
import com.gulfintlauction.Comparators.EndingComparatorDesc;
import com.gulfintlauction.Comparators.PriceComparatorAsc;
import com.gulfintlauction.Comparators.PriceComparatorDesc;
import com.gulfintlauction.Comparators.YearComparatorAsc;
import com.gulfintlauction.Comparators.YearComparatorDesc;
import com.gulfintlauction.R;
import com.gulfintlauction.adapters.CarsAuctionRecyclerViewAdapter;
import com.gulfintlauction.adapters.LedgerRecyclerViewAdapter;
import com.gulfintlauction.models.AppConstant;
import com.gulfintlauction.models.AppModel;
import com.gulfintlauction.models.CarLiveAuctionModel;
import com.gulfintlauction.models.LedgerModel;
import com.gulfintlauction.retrofit.ApiClient;
import com.gulfintlauction.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarsActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    RecyclerView carList;
    ArrayList<CarLiveAuctionModel> data;
    CarsAuctionRecyclerViewAdapter carsAuctionRecyclerViewAdapter;
    RecyclerView.LayoutManager mLinearLayoutManager;
    RecyclerView.LayoutManager mGridLayoutManager;
    Toolbar toolbar;
    TextView filterPrice;
    boolean priceFlag;
    TextView filterYear;
    boolean yearFlag = true;
    TextView filterBids;
    boolean bidsFlag;
    TextView filterEndings;
    boolean endingsFlag;
    ImageView filterChangeView;
    boolean changeViewFlag;
    LinearLayout loader_ll;
    LinearLayout anOtherView;
    LinearLayout ll_login_logout;
    NavigationView navigationView;
    TextView user;
    TextView user_balance;
    TextView logout;

    TextView watchList;
    TextView itemsyoubidding;
    TextView bidsyouwon;
    Menu guestMenu;
    TextView noData;
    Locale locale;

    @Override
    protected void onResume() {
        super.onResume();
        loginlogoutlistner();

        String cusID = AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.SHARED_PREF_CUSTOMER_ID);

        ApiClient.getClient().create(ApiInterface.class).apiGetLadger(cusID).enqueue(new Callback<ArrayList<LedgerModel>>() {

            @Override
            public void onResponse(Call<ArrayList<LedgerModel>> call, Response<ArrayList<LedgerModel>> response) {
                try {
                    user_balance.setText("AED " + response.body().get(response.body().size() - 1).getRemaining_balance());


                } catch (Exception e) {
                    user_balance.setText("AED 0.0");
                }

            }

            @Override
            public void onFailure(Call<ArrayList<LedgerModel>> call, Throwable t) {

            }
        });
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("  " + getString(R.string.cars));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.white_logo);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        data = new ArrayList<>();
        locale = getResources().getConfiguration().locale;


        this.watchList = (TextView) findViewById(R.id.watchlist);
        this.itemsyoubidding = (TextView) findViewById(R.id.itemyoubidding);
        this.bidsyouwon = (TextView) findViewById(R.id.bidYouWon);


        this.filterPrice = (TextView) findViewById(R.id.price);
        this.noData = (TextView) findViewById(R.id.no_data);
        this.filterYear = (TextView) findViewById(R.id.year);
        this.filterBids = (TextView) findViewById(R.id.bids);
        this.filterEndings = (TextView) findViewById(R.id.ending);
        this.filterChangeView = (ImageView) findViewById(R.id.changeView);
        this.carList = (RecyclerView) findViewById(R.id.list_car);
        this.loader_ll = (LinearLayout) findViewById(R.id.loader_ll);
        anOtherView = (LinearLayout) findViewById(R.id.anotherView);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        user = (TextView) navigationView.findViewById(R.id.user);
        user_balance = (TextView) navigationView.findViewById(R.id.user_balance);
        logout = (TextView) navigationView.findViewById(R.id.logout);
        ll_login_logout = (LinearLayout) navigationView.findViewById(R.id.ll_login_logout);

        carsAuctionRecyclerViewAdapter = new CarsAuctionRecyclerViewAdapter(data, this, R.layout.car_live_aution_list_item_new);
        mLinearLayoutManager = new LinearLayoutManager(getApplicationContext());
        mGridLayoutManager = new GridLayoutManager(getApplicationContext(), 2, GridLayoutManager.VERTICAL, false);
        carList.setLayoutManager(mLinearLayoutManager);
        carList.setItemAnimator(new DefaultItemAnimator());
        carList.setAdapter(carsAuctionRecyclerViewAdapter);
        carList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }


            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            }
        });

        this.filterPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDefault();
                if (!priceFlag) {
                    priceFlag = true;
                    filterPrice.setTextColor(getResources().getColor(R.color.colroWhite));
                    filterPrice.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    filterPrice.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.mipmap.arrow_down), null);
                    Collections.sort(data, new PriceComparatorAsc());
                    carsAuctionRecyclerViewAdapter.notifyDataSetChanged();
                } else {
                    priceFlag = false;
                    filterPrice.setTextColor(getResources().getColor(R.color.colorWhite));
                    filterPrice.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    filterPrice.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.mipmap.arrow_up), null);
                    Collections.sort(data, new PriceComparatorDesc());
                    carsAuctionRecyclerViewAdapter.notifyDataSetChanged();
                }
            }
        });
        this.filterYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDefault();
                if (!yearFlag) {
                    yearFlag = true;
                    filterYear.setTextColor(getResources().getColor(R.color.colroWhite));
                    filterYear.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    filterYear.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.mipmap.arrow_down), null);
                    Collections.sort(data, new YearComparatorAsc());
                    carsAuctionRecyclerViewAdapter.notifyDataSetChanged();
                } else {
                    yearFlag = false;
                    filterYear.setTextColor(getResources().getColor(R.color.colroWhite));
                    filterYear.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.mipmap.arrow_up), null);
                    filterYear.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    Collections.sort(data, new YearComparatorDesc());
                    carsAuctionRecyclerViewAdapter.notifyDataSetChanged();
                }

            }
        });
        this.filterBids.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDefault();
                if (!bidsFlag) {
                    bidsFlag = true;
                    filterBids.setTextColor(getResources().getColor(R.color.colroWhite));
                    filterBids.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    filterBids.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.mipmap.arrow_down), null);
                    Collections.sort(data, new BidsComparatorAsc());
                    carsAuctionRecyclerViewAdapter.notifyDataSetChanged();
                } else {
                    bidsFlag = false;
                    filterBids.setTextColor(getResources().getColor(R.color.colroWhite));
                    filterBids.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    filterBids.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.mipmap.arrow_up), null);
                    Collections.sort(data, new BidsComparatorDesc());
                    carsAuctionRecyclerViewAdapter.notifyDataSetChanged();
                }

            }
        });
        this.filterEndings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDefault();
                if (!endingsFlag) {
                    endingsFlag = true;
                    filterEndings.setTextColor(getResources().getColor(R.color.colroWhite));
                    filterEndings.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    Collections.sort(data, new EndingComparatorAsc());
                    carsAuctionRecyclerViewAdapter.notifyDataSetChanged();
                } else {
                    endingsFlag = false;
                    filterEndings.setTextColor(getResources().getColor(R.color.colorPrimary));
                    filterEndings.setBackgroundColor(getResources().getColor(R.color.colroWhite));
                    Collections.sort(data, new EndingComparatorDesc());
                    carsAuctionRecyclerViewAdapter.notifyDataSetChanged();
                }

            }
        });
        this.filterChangeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!changeViewFlag) {
                    changeViewFlag = true;
                    filterChangeView.setImageResource(R.mipmap.list);
                    carsAuctionRecyclerViewAdapter = new CarsAuctionRecyclerViewAdapter(data, CarsActivity.this, R.layout.car_live_aution_list_item_new_1);
                    carList.setLayoutManager(mGridLayoutManager);
                    carList.setItemAnimator(new DefaultItemAnimator());
                    carList.setAdapter(carsAuctionRecyclerViewAdapter);

                } else {
                    changeViewFlag = false;
                    filterChangeView.setImageResource(R.mipmap.grid);
                    carsAuctionRecyclerViewAdapter = new CarsAuctionRecyclerViewAdapter(data, CarsActivity.this, R.layout.car_live_aution_list_item_new);
                    carList.setLayoutManager(mLinearLayoutManager);
                    carList.setItemAnimator(new DefaultItemAnimator());
                    carList.setAdapter(carsAuctionRecyclerViewAdapter);

                }
            }
        });

        ImageView animImageView = (ImageView) findViewById(R.id.animImageView);
        loader_ll.setVisibility(View.VISIBLE);
        try {
            Glide.with(this)
                    .load(R.raw.loader)
                    .into(animImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.ISLOGIN).equals("true")) {
            final Handler handler = new Handler();
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String cusID = AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.SHARED_PREF_CUSTOMER_ID);


                            ApiClient.getClient().create(ApiInterface.class).apiCarLiveAuctions(cusID).enqueue(new Callback<ArrayList<CarLiveAuctionModel>>() {

                                @Override
                                public void onResponse(Call<ArrayList<CarLiveAuctionModel>> call, Response<ArrayList<CarLiveAuctionModel>> response) {
                                    if (response.isSuccessful()) {
                                        data = response.body();
                                        noData.setVisibility(View.GONE);
                                        carsAuctionRecyclerViewAdapter.setData(data);
                                        Collections.sort(data, new YearComparatorAsc());
                                        carsAuctionRecyclerViewAdapter.notifyDataSetChanged();
                                    } else {
                                        noData.setVisibility(View.VISIBLE);
                                    }
                                    loader_ll.setVisibility(View.GONE);
                                }

                                @Override
                                public void onFailure(Call<ArrayList<CarLiveAuctionModel>> call, Throwable t) {
                                    loader_ll.setVisibility(View.GONE);
                                    noData.setVisibility(View.VISIBLE);


                                }
                            });
                            handler.postDelayed(this, 10000);

                        }
                    });
                }
            };
            handler.postDelayed(runnable, 0);


        } else {

            ApiClient.getClient().create(ApiInterface.class).apiCarLiveAuctions().enqueue(new Callback<ArrayList<CarLiveAuctionModel>>() {

                @Override
                public void onResponse(Call<ArrayList<CarLiveAuctionModel>> call, Response<ArrayList<CarLiveAuctionModel>> response) {
                    if (response.isSuccessful()) {
                        data = response.body();
                        noData.setVisibility(View.GONE);
                        carsAuctionRecyclerViewAdapter.setData(data);
                        //Collections.sort(data, new YearComparatorAsc());
                        carsAuctionRecyclerViewAdapter.notifyDataSetChanged();
                    } else {
                        noData.setVisibility(View.VISIBLE);
                    }
                    loader_ll.setVisibility(View.GONE);

                }

                @Override
                public void onFailure(Call<ArrayList<CarLiveAuctionModel>> call, Throwable t) {
                    loader_ll.setVisibility(View.GONE);
                    noData.setVisibility(View.VISIBLE);


                }
            });
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.cars, menu);
        guestMenu = menu;
        try {
            String fullName = AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.SHARED_PREF_F_NAME);
            if (!fullName.equals("false")) {
                menu.getItem(0).setTitle(fullName);
            }
        } catch (Exception e) {

        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    void setDefault() {
        filterPrice.setTextColor(getResources().getColor(R.color.colorPrimary));
        filterPrice.setBackgroundColor(getResources().getColor(R.color.colroWhite));

        filterYear.setTextColor(getResources().getColor(R.color.colorPrimary));
        filterYear.setBackgroundColor(getResources().getColor(R.color.colroWhite));

        filterEndings.setTextColor(getResources().getColor(R.color.colorPrimary));
        filterEndings.setBackgroundColor(getResources().getColor(R.color.colroWhite));

        filterBids.setTextColor(getResources().getColor(R.color.colorPrimary));
        filterBids.setBackgroundColor(getResources().getColor(R.color.colroWhite));
    }

    public void login(View view) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        startActivity(new Intent(this, LoginActivity.class));
    }

    public void signUp(View view) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        startActivity(new Intent(this, RegisterActivity.class));
    }

    public void home(View view) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

    }

    public void liveAuction(View view) {
        Intent intent = new Intent(this, PagesActivity.class);
        intent.putExtra("link", "http://www.gulfintlauction.com/coming_soon_mobile/" + locale);
        intent.putExtra("title", getString(R.string.live_auction));
        startActivity(intent);
    }

    public void comingSoon(View view) {
        Intent intent = new Intent(this, ComingSoonActivity.class);
        startActivity(intent);
    }

    public void howItWork(View view) {
        Intent intent = new Intent(this, PagesActivity.class);
        intent.putExtra("link", "http://www.gulfintlauction.com/how_it_work_mobile/" + locale);
        intent.putExtra("title", getString(R.string.how_it_works));
        startActivity(intent);
    }

    public void contact(View view) {
        Intent intent = new Intent(this, PagesActivity.class);
        intent.putExtra("link", "http://www.gulfintlauction.com/contact_mobile/" + locale);
        intent.putExtra("title", getString(R.string.contact));

        startActivity(intent);
    }

    public void faqs(View view) {
        Intent intent = new Intent(this, PagesActivity.class);
        intent.putExtra("link", "http://www.gulfintlauction.com/faqs_mobile/" + locale);
        intent.putExtra("title", getString(R.string.faqs));
        startActivity(intent);
    }

    public void termConditions(View view) {
        Intent intent = new Intent(this, PagesActivity.class);
        intent.putExtra("link", "http://www.gulfintlauction.com/terms_of_use_mobile/" + locale);
        intent.putExtra("title", getString(R.string.term_amp_conditions));
        startActivity(intent);
    }

    public void refundPolicy(View view) {
        Intent intent = new Intent(this, PagesActivity.class);
        intent.putExtra("link", "http://www.gulfintlauction.com/refund_policy_mobile/" + locale);
        intent.putExtra("title", getString(R.string.refund_policy));
        startActivity(intent);
    }

    public void privacyPolicy(View view) {
        Intent intent = new Intent(this, PagesActivity.class);
        intent.putExtra("link", "http://www.gulfintlauction.com/privacy_policy_mobile/" + locale);
        intent.putExtra("title", getString(R.string.privacy_policy));

        startActivity(intent);
    }

    public void aboutUs(View view) {

        Intent intent = new Intent(this, PagesActivity.class);
        intent.putExtra("link", "http://www.gulfintlauction.com/about_us_mobile/" + locale);
        intent.putExtra("title", getString(R.string.about_us));
        startActivity(intent);
    }

    public void MyLedger(View view) {
        if (AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.ISLOGIN).equals("true")) {

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            startActivity(new Intent(this, LedgerActivity.class));
        } else {
            Toast.makeText(this, "You've to Login first", Toast.LENGTH_SHORT).show();

        }
    }

    public void MyProfile(View view) {
        if (AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.ISLOGIN).equals("true")) {

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            startActivity(new Intent(this, ProfileActivity.class));
        } else {
            Toast.makeText(this, "You've to Login first", Toast.LENGTH_SHORT).show();

        }
    }

    public void claimRefund(View view) {


        if (AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.ISLOGIN).equals("true")) {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            startActivity(new Intent(this, ClaimRefundActivity.class));
        } else {
            Toast.makeText(this, "You've to Login first", Toast.LENGTH_SHORT).show();

        }


    }

    public void logout(View view) {
        logoutlistner();
    }

    public void logoutlistner() {
        AppModel.getInstance().removeSharedPrefrence(getApplicationContext());
        loginlogoutlistner();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        Toast.makeText(CarsActivity.this, "You're logout successfully", Toast.LENGTH_SHORT).show();


    }

    public void loginlogoutlistner() {
        if (AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.ISLOGIN).equals("true")) {
            anOtherView.setVisibility(View.GONE);
            logout.setVisibility(View.VISIBLE);
            ll_login_logout.setVisibility(View.GONE);
            /*final Handler handler = new Handler();
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String cusID = AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.SHARED_PREF_CUSTOMER_ID);


                            ApiClient.getClient().create(ApiInterface.class).apiCarLiveAuctions(cusID).enqueue(new Callback<ArrayList<CarLiveAuctionModel>>() {

                                @Override
                                public void onResponse(Call<ArrayList<CarLiveAuctionModel>> call, Response<ArrayList<CarLiveAuctionModel>> response) {
                                    if (response.isSuccessful()) {
                                        data = response.body();
                                        noData.setVisibility(View.GONE);
                                        carsAuctionRecyclerViewAdapter.setData(data);
                                        Collections.sort(data, new YearComparatorAsc());
                                        carsAuctionRecyclerViewAdapter.notifyDataSetChanged();
                                    } else {
                                        noData.setVisibility(View.VISIBLE);
                                    }
                                    loader_ll.setVisibility(View.GONE);
                                }

                                @Override
                                public void onFailure(Call<ArrayList<CarLiveAuctionModel>> call, Throwable t) {
                                    loader_ll.setVisibility(View.GONE);
                                    noData.setVisibility(View.VISIBLE);


                                }
                            });
                            handler.postDelayed(this, 10000);

                        }
                    });
                }
            };
            handler.postDelayed(runnable, 0);*/


        } else {
            anOtherView.setVisibility(View.VISIBLE);
            logout.setVisibility(View.GONE);
            ll_login_logout.setVisibility(View.VISIBLE);
        }
        try {
            String fullName = AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.SHARED_PREF_F_NAME) + " " + AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.SHARED_PREF_L_NAME);
            if (!fullName.equals("false false")) {
                user.setText(fullName);
            } else {
                user.setText("Guest");
            }
        } catch (Exception e) {

        }
        try {
            String fullName = AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.SHARED_PREF_F_NAME);
            if (!fullName.equals("false")) {
                guestMenu.getItem(0).setTitle(fullName);
            } else {
                guestMenu.getItem(0).setTitle("Guest");
            }
        } catch (Exception e) {

        }
    }

    boolean flag = true;

    public void MyActivity(View view) {
        if (AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.ISLOGIN).equals("true")) {
            if (flag) {
                flag = false;
                watchList.setVisibility(View.VISIBLE);
                itemsyoubidding.setVisibility(View.VISIBLE);
                bidsyouwon.setVisibility(View.VISIBLE);


            } else {
                flag = true;
                watchList.setVisibility(View.GONE);
                itemsyoubidding.setVisibility(View.GONE);
                bidsyouwon.setVisibility(View.GONE);

            }
        } else {
            Toast.makeText(this, "You've to Login first", Toast.LENGTH_SHORT).show();

        }


    }

    public void watchList(View view) {

        startActivity(new Intent(this, WatchListActivity.class));
    }

    public void itemYouBidding(View view) {
        startActivity(new Intent(this, ItemsYouBiddingActivity.class));

    }

    public void bidYouWon(View view) {
        startActivity(new Intent(this, BidsYouWonActivity.class));

    }

    public void makeDeposit(View view) {
        startActivity(new Intent(this, MakeDepositActivity.class));

    }
}
