package com.gulfintlauction.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonObject;
import com.gulfintlauction.R;
import com.gulfintlauction.models.AppConstant;
import com.gulfintlauction.models.AppModel;
import com.gulfintlauction.models.CarLiveAuctionModel;
import com.gulfintlauction.models.LoginModel;
import com.gulfintlauction.retrofit.ApiClient;
import com.gulfintlauction.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText userName;
    private EditText password;
    private Button signIn;
    private RelativeLayout parentLayout;
    LinearLayout loader_ll;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userName = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        signIn = (Button) findViewById(R.id.signIn);
        parentLayout = (RelativeLayout) findViewById(R.id.parentLayout);
        this.loader_ll = (LinearLayout) findViewById(R.id.loader_ll);

        signIn.setOnClickListener(this);
        password.setTransformationMethod(new PasswordTransformationMethod());
    }

    public void register(View v) {
        startActivity(new Intent(this, RegisterActivity.class));
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signIn:
                if (AppModel.getInstance().isValid(parentLayout)) {

                    ImageView animImageView = (ImageView) findViewById(R.id.animImageView);
                    loader_ll.setVisibility(View.VISIBLE);
                    try {
                        Glide.with(this)
                                .load(R.raw.loader)
                                .into(animImageView);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    String refreshedToken = FirebaseInstanceId.getInstance().getToken();

                    ApiClient.getClient().create(ApiInterface.class).apiLogin(userName.getText().toString(), password.getText().toString(),refreshedToken).enqueue(new Callback<LoginModel>() {
                        @Override
                        public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                            if (response.isSuccessful()) {
                                if (response.body().isStatus()) {
                                    Toast.makeText(LoginActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    AppModel.getInstance().writeToSharedPrefrence(getApplicationContext(),AppConstant.USERNAME,userName.getText().toString());
                                    AppModel.getInstance().writeToSharedPrefrence(getApplicationContext(), AppConstant.ISLOGIN, "true");
                                    AppModel.getInstance().writeToSharedPrefrence(getApplicationContext(), AppConstant.SHARED_PREF_F_NAME, response.body().getFirst_name());
                                    AppModel.getInstance().writeToSharedPrefrence(getApplicationContext(), AppConstant.SHARED_PREF_L_NAME, response.body().getLast_name());
                                    AppModel.getInstance().writeToSharedPrefrence(getApplicationContext(), AppConstant.SHARED_PREF_CUSTOMER_ID, response.body().getCustomer_id());
                                    AppModel.getInstance().writeToSharedPrefrence(getApplicationContext(), AppConstant.SHARED_PREF_CUSTOMER_EMAIL, response.body().getEmail_id());
                                    LoginActivity.this.finish();

                                } else {
                                    AppModel.getInstance().writeToSharedPrefrence(getApplicationContext(), AppConstant.ISLOGIN, "false");
                                    Toast.makeText(LoginActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(LoginActivity.this, "Invalid Credential, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                            loader_ll.setVisibility(View.GONE);

                        }

                        @Override
                        public void onFailure(Call<LoginModel> call, Throwable t) {
                            loader_ll.setVisibility(View.GONE);
                            Toast.makeText(LoginActivity.this, "Something went wrong, Please try again!", Toast.LENGTH_SHORT).show();
                        }
                    });

                }
        }
    }
}
