package com.gulfintlauction.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gulfintlauction.R;
import com.gulfintlauction.models.AppConstant;
import com.gulfintlauction.models.AppModel;
import com.gulfintlauction.models.LoginModel;
import com.gulfintlauction.retrofit.ApiClient;
import com.gulfintlauction.retrofit.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText firstName;
    private EditText lastName;
    private EditText userName;
    private EditText password;
    private EditText email;
    private EditText contact;
    LinearLayout loader_ll;
    private TextView tac;
    private CheckBox checkBox;

    private Button signUp;
    private RelativeLayout parentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        firstName = (EditText) findViewById(R.id.firs_name);
        tac = (TextView) findViewById(R.id.tac);
        checkBox = (CheckBox) findViewById(R.id.checkbox);
        tac.setText(Html.fromHtml("<pre>I agree the <font color=\"red\">term & conditions</font> governing the use of this site.</pre>"));
        tac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisterActivity.this, PagesActivity.class);
                intent.putExtra("link", "http://www.gulfintlauction.com/terms_of_use_mobile");
                intent.putExtra("title", "Term & Conditions");
                startActivity(intent);
            }
        });
        lastName = (EditText) findViewById(R.id.last_name);
        userName = (EditText) findViewById(R.id.user_name);
        password = (EditText) findViewById(R.id.password);
        email = (EditText) findViewById(R.id.email);
        contact = (EditText) findViewById(R.id.mobile_number);
        this.loader_ll = (LinearLayout) findViewById(R.id.loader_ll);

        signUp = (Button) findViewById(R.id.signUp);
        parentLayout = (RelativeLayout) findViewById(R.id.parentLayout);

        password.setTransformationMethod(new PasswordTransformationMethod());
        signUp.setOnClickListener(this);


    }

    public void signIn(View v) {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signUp:
                if (AppModel.getInstance().isValid(parentLayout)) {
                    ImageView animImageView = (ImageView) findViewById(R.id.animImageView);
                    loader_ll.setVisibility(View.VISIBLE);
                    try {
                        Glide.with(this)
                                .load(R.raw.loader)
                                .into(animImageView);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    ApiClient.getClient().create(ApiInterface.class).apiRegister(firstName.getText().toString(), lastName.getText().toString(), userName.getText().toString(), password.getText().toString(), email.getText().toString(), contact.getText().toString()).enqueue(new Callback<LoginModel>() {
                        @Override
                        public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                            if (response.isSuccessful()) {
                                if (response.body().isStatus()) {
                                    new AlertDialog.Builder(RegisterActivity.this)
                                            .setMessage("Thank You for Registering with us!\n\n" +
                                                    "Your almost there, please check your email for next step\n" +
                                                    "if you did not receive email in your inbox then don't forget to check your junk/spam folder")
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                                                    RegisterActivity.this.finish();

                                                }
                                            }).show();
                                } else {
                                    Toast.makeText(RegisterActivity.this, "Something went wrong, Please try again!", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(RegisterActivity.this, "Something went wrong, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                            loader_ll.setVisibility(View.GONE);

                            //0Toast.makeText(RegisterActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                            RegisterActivity.this.finish();
                        }

                        @Override
                        public void onFailure(Call<LoginModel> call, Throwable t) {
                            loader_ll.setVisibility(View.GONE);
                            Toast.makeText(RegisterActivity.this, "Something went wrong, Please try again!", Toast.LENGTH_SHORT).show();
                        }
                    });

                }

        }
    }
}
