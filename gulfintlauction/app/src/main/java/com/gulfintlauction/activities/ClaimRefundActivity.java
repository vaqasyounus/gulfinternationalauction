package com.gulfintlauction.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gulfintlauction.R;
import com.gulfintlauction.adapters.LedgerRecyclerViewAdapter;
import com.gulfintlauction.models.AppConstant;
import com.gulfintlauction.models.AppModel;
import com.gulfintlauction.models.ClaimRefundModel;
import com.gulfintlauction.models.LedgerModel;
import com.gulfintlauction.models.WatchModel;
import com.gulfintlauction.retrofit.ApiClient;
import com.gulfintlauction.retrofit.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClaimRefundActivity extends AppCompatActivity {

    ArrayList<LedgerModel> data = new ArrayList<>();
    LinearLayout loader_ll;
    EditText claim_refund_amout;
    TextView remaining_balance;
    Spinner claim_refund_by;
    RadioGroup claim_refund_type;
    RadioButton normal;
    RadioButton urgent;
    Button claimRefund;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_claim_refund);

        remaining_balance = (TextView) findViewById(R.id.remaining_balance);
        claim_refund_amout = (EditText) findViewById(R.id.refund_amount);
        claim_refund_by = (Spinner) findViewById(R.id.refund_claim_by);
        claim_refund_type = (RadioGroup) findViewById(R.id.refund_claim_type);
        normal = (RadioButton) findViewById(R.id.normal);
        urgent = (RadioButton) findViewById(R.id.urgent);
        claimRefund = (Button) findViewById(R.id.claim_refund);

        this.loader_ll = (LinearLayout) findViewById(R.id.loader_ll);


        ImageView animImageView = (ImageView) findViewById(R.id.animImageView);
        loader_ll.setVisibility(View.VISIBLE);
        try {
            Glide.with(this)
                    .load(R.raw.loader)
                    .into(animImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String cusID = AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.SHARED_PREF_CUSTOMER_ID);

        ApiClient.getClient().create(ApiInterface.class).apiGetLadger(cusID).enqueue(new Callback<ArrayList<LedgerModel>>() {

            @Override
            public void onResponse(Call<ArrayList<LedgerModel>> call, Response<ArrayList<LedgerModel>> response) {
                if (response.isSuccessful()) {
                    data = response.body();
                    try {
                        remaining_balance.setText(data.get(data.size() - 1).getRemaining_balance());

                    } catch (Exception e) {
                        remaining_balance.setText("0.0");

                    }
                }
                loader_ll.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ArrayList<LedgerModel>> call, Throwable t) {
                loader_ll.setVisibility(View.GONE);

            }
        });

        claimRefund.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView animImageView = (ImageView) findViewById(R.id.animImageView);
                loader_ll.setVisibility(View.VISIBLE);
                try {
                    Glide.with(ClaimRefundActivity.this)
                            .load(R.raw.loader)
                            .into(animImageView);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String cusID = AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.SHARED_PREF_CUSTOMER_ID);
                String claimRefundType = "";
                if (claim_refund_type.getCheckedRadioButtonId() == R.id.urgent) {
                    claimRefundType = "Urgent";
                } else {
                    claimRefundType = "Normal";

                }

                ApiClient.getClient().create(ApiInterface.class).apiClaimRefund(cusID, claim_refund_amout.getText().toString(), claim_refund_by.getSelectedItem().toString(), claimRefundType).enqueue(new Callback<ClaimRefundModel>() {

                    @Override
                    public void onResponse(Call<ClaimRefundModel> call, Response<ClaimRefundModel> response) {
                        if (response.isSuccessful()) {
                            Toast.makeText(ClaimRefundActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ClaimRefundActivity.this, "Something went wrong, Please try again!", Toast.LENGTH_SHORT).show();
                        }
                        loader_ll.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailure(Call<ClaimRefundModel> call, Throwable t) {
                        loader_ll.setVisibility(View.GONE);
                        Toast.makeText(ClaimRefundActivity.this, "Something went wrong, Please try again!", Toast.LENGTH_SHORT).show();


                    }
                });
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
