package com.gulfintlauction.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gulfintlauction.R;
import com.gulfintlauction.adapters.LedgerRecyclerViewAdapter;
import com.gulfintlauction.models.AppConstant;
import com.gulfintlauction.models.AppModel;
import com.gulfintlauction.models.BankModel;
import com.gulfintlauction.models.LedgerModel;
import com.gulfintlauction.retrofit.ApiClient;
import com.gulfintlauction.retrofit.ApiInterface;
import com.warkiz.widget.IndicatorSeekBar;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MakeDepositActivity extends AppCompatActivity {

    IndicatorSeekBar indicatorSeekBar;
    EditText biddingLimit;
    EditText amoutPay;
    Button pay;
    Button paybycheck;
    LinearLayout loader_ll;
    TextView remaining_balance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_deposit);

        biddingLimit = (EditText) findViewById(R.id.bidding_limit);
        amoutPay = (EditText) findViewById(R.id.amount_pay);
        indicatorSeekBar = (IndicatorSeekBar) findViewById(R.id.seekbar);
        pay = (Button) findViewById(R.id.pay);
        paybycheck = (Button) findViewById(R.id.paybycheck);
        remaining_balance = (TextView) findViewById(R.id.remaining_balance);

        this.loader_ll = (LinearLayout) findViewById(R.id.loader_ll);
        ImageView animImageView = (ImageView) findViewById(R.id.animImageView);

        loader_ll.setVisibility(View.VISIBLE);
        try {
            Glide.with(this)
                    .load(R.raw.loader)
                    .into(animImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String cusID = AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.SHARED_PREF_CUSTOMER_ID);

        ApiClient.getClient().create(ApiInterface.class).apiGetLadger(cusID).enqueue(new Callback<ArrayList<LedgerModel>>() {

            @Override
            public void onResponse(Call<ArrayList<LedgerModel>> call, Response<ArrayList<LedgerModel>> response) {
                try {
                    remaining_balance.setText(response.body().get(response.body().size() - 1).getRemaining_balance());

                } catch (Exception e) {
                    remaining_balance.setText("0.0");

                }
                loader_ll.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ArrayList<LedgerModel>> call, Throwable t) {
                loader_ll.setVisibility(View.GONE);

            }
        });

        indicatorSeekBar.setOnSeekChangeListener(new IndicatorSeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(IndicatorSeekBar seekBar, int progress, float progressFloat, boolean fromUserTouch) {

                biddingLimit.setText(String.valueOf(progress));
                amoutPay.setText(String.valueOf((int) (progress * 0.1)));
            }

            @Override
            public void onSectionChanged(IndicatorSeekBar seekBar, int thumbPosOnTick, String textBelowTick, boolean fromUserTouch) {
                //only callback on discrete series SeekBar type.
            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar, int thumbPosOnTick) {
            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {
            }
        });
        paybycheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MakeDepositActivity.this, DepositWebpageActivity.class);
                intent.putExtra("link", "http://gulfintlauction.com/cheque_mobile/" + getResources().getConfiguration().locale);
                intent.putExtra("title", "Pay By Check");
                startActivity(intent);
            }
        });
        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ImageView animImageView = (ImageView) findViewById(R.id.animImageView);

                loader_ll.setVisibility(View.VISIBLE);
                try {
                    Glide.with(MakeDepositActivity.this)
                            .load(R.raw.loader)
                            .into(animImageView);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String cusID = AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.SHARED_PREF_CUSTOMER_ID);

                ApiClient.getClient().create(ApiInterface.class).apimakeDeposit(amoutPay.getText().toString().trim(), cusID).enqueue(new Callback<BankModel>() {

                    @Override
                    public void onResponse(Call<BankModel> call, Response<BankModel> response) {
                        if (response.isSuccessful()) {
                            if (response.body().isStatus()) {
                                //Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(response.body().getBank_link()));
                                //startActivity(browserIntent);

                                Intent intent = new Intent(MakeDepositActivity.this, DepositWebpageActivity.class);
                                intent.putExtra("link", response.body().getBank_link());
                                intent.putExtra("title", "Pay Online");
                                startActivity(intent);
                            } else {

                            }
                        } else {
                            Toast.makeText(MakeDepositActivity.this, "Please insert minimum AED 2500/=", Toast.LENGTH_SHORT).show();

                        }

                        loader_ll.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailure(Call<BankModel> call, Throwable t) {
                        loader_ll.setVisibility(View.GONE);
                        Toast.makeText(MakeDepositActivity.this, "Something went wrong, Please try again!", Toast.LENGTH_SHORT).show();

                    }
                });

            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
