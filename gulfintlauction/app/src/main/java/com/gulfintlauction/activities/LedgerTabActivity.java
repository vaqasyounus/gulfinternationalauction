package com.gulfintlauction.activities;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gulfintlauction.R;
import com.gulfintlauction.models.AppConstant;
import com.gulfintlauction.models.AppModel;
import com.gulfintlauction.models.LedgerModel;
import com.gulfintlauction.retrofit.ApiClient;
import com.gulfintlauction.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import com.gulfintlauction.fragments.AllTransactionFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LedgerTabActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    ArrayList<LedgerModel> data = new ArrayList<>();
    LinearLayout loader_ll;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ledger_tab);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
//        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
//        tabLayout.setupWithViewPager(viewPager);

        this.loader_ll = (LinearLayout) findViewById(R.id.loader_ll);


        ImageView animImageView = (ImageView) findViewById(R.id.animImageView);
        loader_ll.setVisibility(View.VISIBLE);
        try {
            Glide.with(this)
                    .load(R.raw.loader)
                    .into(animImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String cusID = AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.SHARED_PREF_CUSTOMER_ID);
        ApiClient.getClient().create(ApiInterface.class).apiGetLadger(cusID).enqueue(new Callback<ArrayList<LedgerModel>>() {

            @Override
            public void onResponse(Call<ArrayList<LedgerModel>> call, Response<ArrayList<LedgerModel>> response) {
                if (response.isSuccessful()) {
                    data = response.body();

                }else{
                    Toast.makeText(LedgerTabActivity.this, "Something went wrong, Please try again!", Toast.LENGTH_SHORT).show();
                }
                setupViewPager(viewPager);
                tabLayout.setupWithViewPager(viewPager);
                loader_ll.setVisibility(View.GONE);


            }

            @Override
            public void onFailure(Call<ArrayList<LedgerModel>> call, Throwable t) {
                Toast.makeText(LedgerTabActivity.this, "Network failure, Please try again!", Toast.LENGTH_SHORT).show();
                loader_ll.setVisibility(View.GONE);

            }
        });

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new AllTransactionFragment(data), "All Transactions");
        adapter.addFragment(new AllTransactionFragment(data), "Credit");
        adapter.addFragment(new AllTransactionFragment(data), "Debit");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}