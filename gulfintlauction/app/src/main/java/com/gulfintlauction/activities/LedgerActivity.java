package com.gulfintlauction.activities;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gulfintlauction.Comparators.YearComparatorAsc;
import com.gulfintlauction.R;
import com.gulfintlauction.adapters.CarsAuctionRecyclerViewAdapter;
import com.gulfintlauction.adapters.LedgerRecyclerViewAdapter;
import com.gulfintlauction.models.AppConstant;
import com.gulfintlauction.models.AppModel;
import com.gulfintlauction.models.CarLiveAuctionModel;
import com.gulfintlauction.models.LedgerModel;
import com.gulfintlauction.retrofit.ApiClient;
import com.gulfintlauction.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.Collections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LedgerActivity extends AppCompatActivity {

    RecyclerView ledgerList;
    ArrayList<LedgerModel> data = new ArrayList<>();
    LedgerRecyclerViewAdapter ledgerRecyclerViewAdapter;
    RecyclerView.LayoutManager mLinearLayoutManager;
    LinearLayout loader_ll;
    TextView remaining_balance;

    public LedgerRecyclerViewAdapter getLedgerRecyclerViewAdapter() {
        return ledgerRecyclerViewAdapter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ledger);

        remaining_balance = (TextView) findViewById(R.id.remaining_balance);

        this.loader_ll = (LinearLayout) findViewById(R.id.loader_ll);


        ImageView animImageView = (ImageView) findViewById(R.id.animImageView);
        loader_ll.setVisibility(View.VISIBLE);
        try {
            Glide.with(this)
                    .load(R.raw.loader)
                    .into(animImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String cusID = AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.SHARED_PREF_CUSTOMER_ID);

        ApiClient.getClient().create(ApiInterface.class).apiGetLadger(cusID).enqueue(new Callback<ArrayList<LedgerModel>>() {

            @Override
            public void onResponse(Call<ArrayList<LedgerModel>> call, Response<ArrayList<LedgerModel>> response) {
                data = response.body();
                try {
                    remaining_balance.setText(data.get(data.size() - 1).getRemaining_balance());

                } catch (Exception e) {
                    remaining_balance.setText("0.0");

                }
                ledgerRecyclerViewAdapter = new LedgerRecyclerViewAdapter(data, LedgerActivity.this);
                mLinearLayoutManager = new LinearLayoutManager(getApplicationContext());
                ledgerList = (RecyclerView) findViewById(R.id.list_ledger);
                ledgerList.setLayoutManager(mLinearLayoutManager);
                ledgerList.setItemAnimator(new DefaultItemAnimator());
                ledgerList.setAdapter(ledgerRecyclerViewAdapter);
                loader_ll.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ArrayList<LedgerModel>> call, Throwable t) {
                loader_ll.setVisibility(View.GONE);

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
