package com.gulfintlauction.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.gulfintlauction.R;
import com.gulfintlauction.models.AppConstant;
import com.gulfintlauction.models.AppModel;
import com.gulfintlauction.models.BidModel;
import com.gulfintlauction.models.CarLiveAuctionModel;
import com.gulfintlauction.models.WatchModel;
import com.gulfintlauction.retrofit.ApiClient;
import com.gulfintlauction.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ComingSoonDetailActivity extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    private SliderLayout mDemoSlider;
    LinearLayout loader_ll;
    //    LinearLayout anOtherView;
    Toolbar toolbar;
    TextView cur_price;
    TextView min_inc;
    TextView last_bid;
    TextView time_remaining;
    CountDownTimer mCountDownTimer;
    TextView no_of_bids;
    int id;
    Button share;
    TextView inquireNow;
    TextView specification;
    TextView map;
    TextView details;
    CarLiveAuctionModel carLiveAuctionModel;

    @Override
    protected void onResume() {
        super.onResume();
        if (AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.ISLOGIN).equals("true")) {
//            anOtherView.setVisibility(View.GONE);
        } else {
//            anOtherView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coming_soon_detail);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.cars));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("Lot # " + getIntent().getStringExtra("lot"));
        AppModel.getInstance().images = new ArrayList<>();


        mDemoSlider = (SliderLayout) findViewById(R.id.slider);
        share = (Button) findViewById(R.id.share);
        cur_price = (TextView) findViewById(R.id.current_price);
        min_inc = (TextView) findViewById(R.id.min_inc);
        last_bid = (TextView) findViewById(R.id.last_bid);
        time_remaining = (TextView) findViewById(R.id.time_remaining);
        no_of_bids = (TextView) findViewById(R.id.no_of_bids);
        inquireNow = (TextView) findViewById(R.id.inquire_now);
        specification = (TextView) findViewById(R.id.specification);
        map = (TextView) findViewById(R.id.map);
        details = (TextView) findViewById(R.id.details);
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ComingSoonDetailActivity.this, PagesActivity.class);
                intent.putExtra("link", "http://www.gulfintlauction.com/map_mobile/" + carLiveAuctionModel.getId());
                intent.putExtra("title", "Location");
                startActivity(intent);
            }
        });

        specification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppModel.getInstance().data = carLiveAuctionModel;
                Intent intent = new Intent(ComingSoonDetailActivity.this, SpecificationActivity.class);
                intent.putExtra("title",carLiveAuctionModel.getLot_no());
                startActivity(intent);
            }
        });
        inquireNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ComingSoonDetailActivity.this, PagesActivity.class);
                String url = "http://www.gulfintlauction.com/mobile/inquiry_mobile/" + carLiveAuctionModel.getId();
                intent.putExtra("link", url);
                intent.putExtra("title", "Send Enquiry");
                startActivity(intent);
            }
        });

//        anOtherView = (LinearLayout) findViewById(R.id.anotherView);

        this.loader_ll = (LinearLayout) findViewById(R.id.loader_ll);
        final ImageView animImageView = (ImageView) findViewById(R.id.animImageView);
        loader_ll.setVisibility(View.VISIBLE);
        try {
            Glide.with(this)
                    .load(R.raw.loader)
                    .into(animImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        id = getIntent().getIntExtra("id", 0);
        String cusID = AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.SHARED_PREF_CUSTOMER_ID);
        final HashMap<String, String> url_maps = new HashMap<String, String>();


        ApiClient.getClient().create(ApiInterface.class).apiComingSoon(id).enqueue(new Callback<ArrayList<CarLiveAuctionModel>>() {

            @Override
            public void onResponse(Call<ArrayList<CarLiveAuctionModel>> call, Response<ArrayList<CarLiveAuctionModel>> response) {
                ArrayList<CarLiveAuctionModel> carLiveAuctionModels = response.body();
                Log.d("waqas", response.body().toString());
                carLiveAuctionModel = carLiveAuctionModels.get(0);
                cur_price.setText("AED " + carLiveAuctionModel.getCurrent_price());
                min_inc.setText("AED " + carLiveAuctionModel.getMin_bid_increment());
                last_bid.setText("AED " + carLiveAuctionModel.getLast_bid());
                no_of_bids.setText(" " + carLiveAuctionModel.getTotal_bids());
                details.setText("Remarks : "+carLiveAuctionModel.getRemarks());
                long diff = Long.parseLong(carLiveAuctionModel.getEnd_date_time()) - System.currentTimeMillis();
                mCountDownTimer = new CountDownTimer(diff, 1000) {
                    StringBuilder time = new StringBuilder();

                    @Override
                    public void onFinish() {
                        time_remaining.setText(DateUtils.formatElapsedTime(0));
                    }

                    @Override
                    public void onTick(long millisUntilFinished) {
                        time.setLength(0);
                        if (millisUntilFinished > DateUtils.DAY_IN_MILLIS) {
                            long count = millisUntilFinished / DateUtils.DAY_IN_MILLIS;
                            if (count > 1)
                                time.append(count).append(" days ");
                            else
                                time.append(count).append(" day ");
                            millisUntilFinished %= DateUtils.DAY_IN_MILLIS;
                        }
                        time.append(DateUtils.formatElapsedTime(Math.round(millisUntilFinished / 1000d)));
                        time_remaining.setText(" " + time.toString());

                    }
                }.start();


                List<String> images = carLiveAuctionModels.get(0).getImages();
                for (int i = 0; i < images.size(); i++) {
                    AppModel.getInstance().images.add(images.get(i));
                    url_maps.put(carLiveAuctionModels.get(0).getModel_name() + " " + (i + 1), images.get(i));
                }
                loader_ll.setVisibility(View.GONE);
                for (String name : url_maps.keySet()) {
                    TextSliderView textSliderView = new TextSliderView(ComingSoonDetailActivity.this);
                    // initialize a SliderLayout
                    textSliderView
                            .description(name)
                            .image(url_maps.get(name))
                            .setScaleType(BaseSliderView.ScaleType.Fit)
                            .setOnSliderClickListener(ComingSoonDetailActivity.this);


                    //add your extra information
                    textSliderView.bundle(new Bundle());
                    textSliderView.getBundle()
                            .putString("extra", name);

                    mDemoSlider.addSlider(textSliderView);
                }
                mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
                mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                mDemoSlider.setCustomAnimation(new DescriptionAnimation());
                mDemoSlider.setDuration(5000);
                mDemoSlider.addOnPageChangeListener(ComingSoonDetailActivity.this);
            }

            @Override
            public void onFailure(Call<ArrayList<CarLiveAuctionModel>> call, Throwable t) {
                loader_ll.setVisibility(View.GONE);

            }
        });




        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                share.putExtra(Intent.EXTRA_SUBJECT, "Gulf International Auction");
                share.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.gulfintlauction");
                startActivity(Intent.createChooser(share, "Gulf International Auction"));
            }
        });


    }

    @Override
    protected void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        mDemoSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        startActivity(new Intent(ComingSoonDetailActivity.this, ImageViewPagerActivity.class));
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }
}