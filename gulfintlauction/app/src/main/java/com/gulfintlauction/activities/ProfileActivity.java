package com.gulfintlauction.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.PointerIcon;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gulfintlauction.R;
import com.gulfintlauction.models.AppConstant;
import com.gulfintlauction.models.AppModel;
import com.gulfintlauction.models.LoginModel;
import com.gulfintlauction.models.ProfileModel;
import com.gulfintlauction.models.ProfileUpdateModel;
import com.gulfintlauction.retrofit.ApiClient;
import com.gulfintlauction.retrofit.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {
    MenuItem itemEdit;

    Spinner title;
    EditText firstName;
    EditText lastName;
    EditText password;
    EditText contact;
    EditText dob;
    RadioGroup gender;
    RadioButton male, female;
    Spinner nationality;
    RadioGroup maritalStatus;
    RadioButton notMarried, married;
    RadioGroup addressType;
    RadioButton home, bussiness;
    EditText addressLine1;
    EditText addressLine2;
    EditText cityTown;
    EditText stateProvice;
    EditText postcode;
    EditText passportNumber;
    Spinner issueCountry;
    EditText passportExpDate;
    EditText emiratesID;
    EditText emiratesExpDate;

    ImageView passport_photo;
    ImageView emirates_photo;

    TextView attach_passport_btn;
    TextView attach_emirates_btn;

    TextView verifyPassportNum;
    TextView verifyEmirateId;

    Button update;
    LinearLayout parentLayout;

    LinearLayout loader_ll;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        title = (Spinner) findViewById(R.id.title);
        firstName = (EditText) findViewById(R.id.first_name);
        lastName = (EditText) findViewById(R.id.last_name);
        password = (EditText) findViewById(R.id.password);
        contact = (EditText) findViewById(R.id.contact);
        dob = (EditText) findViewById(R.id.dob);
        gender = (RadioGroup) findViewById(R.id.gender);
        nationality = (Spinner) findViewById(R.id.nationality);
        maritalStatus = (RadioGroup) findViewById(R.id.marital_status);
        addressType = (RadioGroup) findViewById(R.id.address_type);
        addressLine1 = (EditText) findViewById(R.id.address_line1);
        addressLine2 = (EditText) findViewById(R.id.address_line2);
        cityTown = (EditText) findViewById(R.id.city_town);
        stateProvice = (EditText) findViewById(R.id.state_province);
        postcode = (EditText) findViewById(R.id.post_code);
        passportNumber = (EditText) findViewById(R.id.passport_num);
        issueCountry = (Spinner) findViewById(R.id.issue_country);
        passportExpDate = (EditText) findViewById(R.id.pas_exp_date);
        emiratesID = (EditText) findViewById(R.id.emirates_id);
        emiratesExpDate = (EditText) findViewById(R.id.emirates_exp_date);

        emirates_photo = (ImageView) findViewById(R.id.attach_emirates_id_img);
        passport_photo = (ImageView) findViewById(R.id.attach_passport_img);

        attach_emirates_btn = (TextView) findViewById(R.id.attach_emirates_id_btn);
        attach_passport_btn = (TextView) findViewById(R.id.attach_passport_btn);


        verifyPassportNum = (TextView) findViewById(R.id.passport_num_verify);
        verifyEmirateId = (TextView) findViewById(R.id.emirate_id_verify);

        update = (Button) findViewById(R.id.update);
        parentLayout = (LinearLayout) findViewById(R.id.parentLayout2);

        male = (RadioButton) findViewById(R.id.male);
        female = (RadioButton) findViewById(R.id.female);
        notMarried = (RadioButton) findViewById(R.id.not_married);
        married = (RadioButton) findViewById(R.id.married);
        home = (RadioButton) findViewById(R.id.home);
        bussiness = (RadioButton) findViewById(R.id.bussiness);

        this.loader_ll = (LinearLayout) findViewById(R.id.loader_ll);


        disable(true);


        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (password.getText().toString().trim().length() >= 1) {
                    password.setError(null);
                    itemEdit.setVisible(true);
                    disable(true);
                    ImageView animImageView = (ImageView) findViewById(R.id.animImageView);
                    loader_ll.setVisibility(View.VISIBLE);
                    try {
                        Glide.with(ProfileActivity.this)
                                .load(R.raw.loader)
                                .into(animImageView);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    String cusID = AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.SHARED_PREF_CUSTOMER_ID);
                    String sGender = ((RadioButton) findViewById(gender.getCheckedRadioButtonId())).getText().toString();
                    String sMaritalStatus = ((RadioButton) findViewById(maritalStatus.getCheckedRadioButtonId())).getText().toString();
                    String sAddressType = ((RadioButton) findViewById(addressType.getCheckedRadioButtonId())).getText().toString();


                    ApiClient.getClient().create(ApiInterface.class).apiUpdateProfile(cusID, title.getSelectedItem().toString(), get(firstName), get(lastName), get(contact), get(dob), sGender, nationality.getSelectedItem().toString(), sMaritalStatus, get(password), sAddressType, get(addressLine1), get(addressLine2), get(cityTown), get(stateProvice), get(postcode), get(passportNumber), issueCountry.getSelectedItem().toString(), "", get(passportExpDate), get(emiratesID), "", get(emiratesExpDate)).enqueue(new Callback<ProfileUpdateModel>() {
                        @Override
                        public void onResponse(Call<ProfileUpdateModel> call, Response<ProfileUpdateModel> response) {
                            if (response.isSuccessful() && response.body().isStatus()) {
                                loader_ll.setVisibility(View.GONE);
                                Toast.makeText(ProfileActivity.this, "Profile updated successfully", Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(ProfileActivity.this, "Something went wrong, Please try again!", Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<ProfileUpdateModel> call, Throwable t) {
                            loader_ll.setVisibility(View.GONE);
                            Toast.makeText(ProfileActivity.this, "Something went wrong, Please try again!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    password.setError("Required");
                    password.requestFocus();
                    Toast.makeText(ProfileActivity.this, "Password Field shouldn't be empty", Toast.LENGTH_SHORT).show();
                }

            }
        });


        ImageView animImageView = (ImageView) findViewById(R.id.animImageView);
        loader_ll.setVisibility(View.VISIBLE);
        try {
            Glide.with(this)
                    .load(R.raw.loader)
                    .into(animImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String cusID = AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.SHARED_PREF_CUSTOMER_ID);

        ApiClient.getClient().create(ApiInterface.class).apiGetProfile(cusID).enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                if (response.isSuccessful() && response.body().getStatus()) {
                    loader_ll.setVisibility(View.GONE);
                    ProfileModel pf = response.body();
                    int posTitle = (pf.getTitle().equalsIgnoreCase("Mr.") ? 0 : pf.getTitle().equalsIgnoreCase("Mrs") ? 1 : 2);
                    title.setSelection(posTitle);
                    firstName.setText(pf.getFirst_name());
                    lastName.setText(pf.getLast_name());
                    contact.setText(pf.getContact_no());
                    dob.setText(pf.getDate_of_birth());
                    if (pf.getGender().toLowerCase().equalsIgnoreCase("Male".toLowerCase())) {
                        male.setChecked(true);
                    } else {
                        female.setChecked(true);
                    }
                    if (pf.getMarital_status().toLowerCase().equalsIgnoreCase("Not Married".toLowerCase())) {
                        notMarried.setChecked(true);
                    } else {
                        married.setChecked(true);
                    }
                    if (pf.getAddress_type().toLowerCase().contains("Home".toLowerCase())) {
                        home.setChecked(true);
                    } else {
                        bussiness.setChecked(true);
                    }
                    addressLine1.setText(pf.getAddress());
                    addressLine2.setText(pf.getAddress_optional());
                    cityTown.setText(pf.getCity_town());
                    stateProvice.setText(pf.getState_province());
                    postcode.setText(pf.getPost_code());
                    passportNumber.setText(pf.getPassport_no());
                    passportExpDate.setText(pf.getPassport_expiry_date());
                    emiratesID.setText(pf.getEmirates_id());
                    emiratesExpDate.setText(pf.getEmirates_expiry_date());

                    if (pf.getPassport_approved().equals("1")) {
                        verifyPassportNum.setText("Verified");
                        verifyPassportNum.setTextColor(Color.GREEN);
                    } else {
                        verifyPassportNum.setText("Not Verified");
                        verifyPassportNum.setTextColor(Color.RED);

                    }
                    if (pf.getEmirates_id_approved().equals("1")) {
                        verifyEmirateId.setText("Verified");
                        verifyEmirateId.setTextColor(Color.GREEN);
                    } else {
                        verifyEmirateId.setText("Not Verified");
                        verifyEmirateId.setTextColor(Color.RED);

                    }

                } else {
                    Toast.makeText(ProfileActivity.this, "Something went wrong, Please try again!", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                loader_ll.setVisibility(View.GONE);
                Toast.makeText(ProfileActivity.this, "Something went wrong, Please try again!", Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        } else if (id == R.id.action_Edit) {
            itemEdit = item;
            disable(false);
            item.setVisible(false);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void disable(boolean bool) {
        if (bool) {
            AppModel.getInstance().disable(parentLayout, true);
            update.setVisibility(View.INVISIBLE);
            title.setEnabled(false);
            nationality.setEnabled(false);
            issueCountry.setEnabled(false);

        } else {
            AppModel.getInstance().disable(parentLayout, false);
            update.setVisibility(View.VISIBLE);
            title.setEnabled(true);
            nationality.setEnabled(true);
            issueCountry.setEnabled(true);
            firstName.requestFocus();

//            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.showSoftInput(firstName, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public String get(EditText et) {
        return et.getText().toString().trim();
    }
}
