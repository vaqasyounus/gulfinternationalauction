package com.gulfintlauction.activities;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gulfintlauction.Comparators.BidsComparatorAsc;
import com.gulfintlauction.Comparators.BidsComparatorDesc;
import com.gulfintlauction.Comparators.EndingComparatorAsc;
import com.gulfintlauction.Comparators.EndingComparatorDesc;
import com.gulfintlauction.Comparators.PriceComparatorAsc;
import com.gulfintlauction.Comparators.PriceComparatorDesc;
import com.gulfintlauction.Comparators.YearComparatorAsc;
import com.gulfintlauction.Comparators.YearComparatorDesc;
import com.gulfintlauction.R;
import com.gulfintlauction.adapters.CarsAuctionRecyclerViewAdapter;
import com.gulfintlauction.models.AppConstant;
import com.gulfintlauction.models.AppModel;
import com.gulfintlauction.models.CarLiveAuctionModel;
import com.gulfintlauction.retrofit.ApiClient;
import com.gulfintlauction.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.Collections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WatchListActivity extends AppCompatActivity {

    RecyclerView carList;
    ArrayList<CarLiveAuctionModel> data;
    CarsAuctionRecyclerViewAdapter carsAuctionRecyclerViewAdapter;
    RecyclerView.LayoutManager mLinearLayoutManager;
    RecyclerView.LayoutManager mGridLayoutManager;
    Toolbar toolbar;
    TextView filterPrice;
    boolean priceFlag;
    TextView filterYear;
    boolean yearFlag = true;
    TextView filterBids;
    boolean bidsFlag;
    TextView filterEndings;
    boolean endingsFlag;
    ImageView filterChangeView;
    boolean changeViewFlag;
    LinearLayout loader_ll;
    LinearLayout anOtherView;
    LinearLayout ll_login_logout;
    TextView logout;
    TextView noData;


    @Override
    protected void onResume() {
        super.onResume();
        loginlogoutlistner();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watch_list);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        data = new ArrayList<>();
        this.noData = (TextView) findViewById(R.id.no_data);
        this.filterPrice = (TextView) findViewById(R.id.price);
        this.filterYear = (TextView) findViewById(R.id.year);
        this.filterBids = (TextView) findViewById(R.id.bids);
        this.filterEndings = (TextView) findViewById(R.id.ending);
        this.filterChangeView = (ImageView) findViewById(R.id.changeView);
        this.carList = (RecyclerView) findViewById(R.id.list_car);
        this.loader_ll = (LinearLayout) findViewById(R.id.loader_ll);
        anOtherView = (LinearLayout) findViewById(R.id.anotherView);

        carsAuctionRecyclerViewAdapter = new CarsAuctionRecyclerViewAdapter(data, WatchListActivity.this, R.layout.car_live_aution_list_item_new);
        mLinearLayoutManager = new LinearLayoutManager(getApplicationContext());
        mGridLayoutManager = new GridLayoutManager(getApplicationContext(), 2, GridLayoutManager.VERTICAL, false);
        carList.setLayoutManager(mLinearLayoutManager);
        carList.setItemAnimator(new DefaultItemAnimator());
        carList.setAdapter(carsAuctionRecyclerViewAdapter);
        carList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }


            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

            }
        });

        this.filterPrice.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                setDefault();
                if (!priceFlag) {
                    priceFlag = true;
                    filterPrice.setTextColor(getResources().getColor(R.color.colroWhite));
                    filterPrice.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    filterPrice.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.mipmap.arrow_down), null);
                    Collections.sort(data, new PriceComparatorAsc());
                    carsAuctionRecyclerViewAdapter.notifyDataSetChanged();
                } else {
                    priceFlag = false;
                    filterPrice.setTextColor(getResources().getColor(R.color.colorWhite));
                    filterPrice.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    filterPrice.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.mipmap.arrow_up), null);
                    Collections.sort(data, new PriceComparatorDesc());
                    carsAuctionRecyclerViewAdapter.notifyDataSetChanged();
                }
            }
        });
        this.filterYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDefault();
                if (!yearFlag) {
                    yearFlag = true;
                    filterYear.setTextColor(getResources().getColor(R.color.colroWhite));
                    filterYear.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    filterYear.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.mipmap.arrow_down), null);
                    Collections.sort(data, new YearComparatorAsc());
                    carsAuctionRecyclerViewAdapter.notifyDataSetChanged();
                } else {
                    yearFlag = false;
                    filterYear.setTextColor(getResources().getColor(R.color.colroWhite));
                    filterYear.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.mipmap.arrow_up), null);
                    filterYear.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    Collections.sort(data, new YearComparatorDesc());
                    carsAuctionRecyclerViewAdapter.notifyDataSetChanged();
                }

            }
        });
        this.filterBids.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDefault();
                if (!bidsFlag) {
                    bidsFlag = true;
                    filterBids.setTextColor(getResources().getColor(R.color.colroWhite));
                    filterBids.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    filterBids.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.mipmap.arrow_down), null);
                    Collections.sort(data, new BidsComparatorAsc());
                    carsAuctionRecyclerViewAdapter.notifyDataSetChanged();
                } else {
                    bidsFlag = false;
                    filterBids.setTextColor(getResources().getColor(R.color.colroWhite));
                    filterBids.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    filterBids.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.mipmap.arrow_up), null);
                    Collections.sort(data, new BidsComparatorDesc());
                    carsAuctionRecyclerViewAdapter.notifyDataSetChanged();
                }

            }
        });
        this.filterEndings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDefault();
                if (!endingsFlag) {
                    endingsFlag = true;
                    filterEndings.setTextColor(getResources().getColor(R.color.colroWhite));
                    filterEndings.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    Collections.sort(data, new EndingComparatorAsc());
                    carsAuctionRecyclerViewAdapter.notifyDataSetChanged();
                } else {
                    endingsFlag = false;
                    filterEndings.setTextColor(getResources().getColor(R.color.colorPrimary));
                    filterEndings.setBackgroundColor(getResources().getColor(R.color.colroWhite));
                    Collections.sort(data, new EndingComparatorDesc());
                    carsAuctionRecyclerViewAdapter.notifyDataSetChanged();
                }

            }
        });
        this.filterChangeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!changeViewFlag) {
                    changeViewFlag = true;
                    filterChangeView.setImageResource(R.mipmap.list);
                    carsAuctionRecyclerViewAdapter = new CarsAuctionRecyclerViewAdapter(data, WatchListActivity.this, R.layout.car_live_aution_list_item_new_1);
                    carList.setLayoutManager(mGridLayoutManager);
                    carList.setItemAnimator(new DefaultItemAnimator());
                    carList.setAdapter(carsAuctionRecyclerViewAdapter);

                } else {
                    changeViewFlag = false;
                    filterChangeView.setImageResource(R.mipmap.grid);
                    carsAuctionRecyclerViewAdapter = new CarsAuctionRecyclerViewAdapter(data, WatchListActivity.this, R.layout.car_live_aution_list_item_new);
                    carList.setLayoutManager(mLinearLayoutManager);
                    carList.setItemAnimator(new DefaultItemAnimator());
                    carList.setAdapter(carsAuctionRecyclerViewAdapter);

                }
            }
        });

        ImageView animImageView = (ImageView) findViewById(R.id.animImageView);
        loader_ll.setVisibility(View.VISIBLE);
        try {
            Glide.with(this)
                    .load(R.raw.loader)
                    .into(animImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String cusID = AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.SHARED_PREF_CUSTOMER_ID);

        ApiClient.getClient().create(ApiInterface.class).apiMyWatchList(cusID).enqueue(new Callback<ArrayList<CarLiveAuctionModel>>() {

            @Override
            public void onResponse(Call<ArrayList<CarLiveAuctionModel>> call, Response<ArrayList<CarLiveAuctionModel>> response) {
                if (response.isSuccessful()) {
                    data = response.body();
                    noData.setVisibility(View.GONE);
                    carsAuctionRecyclerViewAdapter.setData(data);
                    Collections.sort(data, new YearComparatorAsc());
                    carsAuctionRecyclerViewAdapter.notifyDataSetChanged();
                } else {
                    noData.setVisibility(View.VISIBLE);
                }
                loader_ll.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<ArrayList<CarLiveAuctionModel>> call, Throwable t) {
                loader_ll.setVisibility(View.GONE);
                noData.setVisibility(View.VISIBLE);


            }
        });
    }


    void setDefault() {
        filterPrice.setTextColor(getResources().getColor(R.color.colorPrimary));
        filterPrice.setBackgroundColor(getResources().getColor(R.color.colroWhite));

        filterYear.setTextColor(getResources().getColor(R.color.colorPrimary));
        filterYear.setBackgroundColor(getResources().getColor(R.color.colroWhite));

        filterEndings.setTextColor(getResources().getColor(R.color.colorPrimary));
        filterEndings.setBackgroundColor(getResources().getColor(R.color.colroWhite));

        filterBids.setTextColor(getResources().getColor(R.color.colorPrimary));
        filterBids.setBackgroundColor(getResources().getColor(R.color.colroWhite));
    }

    public void login(View view) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        startActivity(new Intent(this, LoginActivity.class));
    }

    public void signUp(View view) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        startActivity(new Intent(this, RegisterActivity.class));
    }


    public void loginlogoutlistner() {
        if (AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.ISLOGIN).equals("true")) {
            anOtherView.setVisibility(View.GONE);
        } else {
            anOtherView.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
