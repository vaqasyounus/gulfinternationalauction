package com.gulfintlauction.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gulfintlauction.R;
import com.gulfintlauction.helpers.App;
import com.gulfintlauction.helpers.LocaleHelper;
import com.gulfintlauction.models.AppConstant;
import com.gulfintlauction.models.AppModel;
import com.gulfintlauction.models.CountModel;
import com.gulfintlauction.retrofit.RestApis;
import com.gulfintlauction.services.BadgeIntentService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Locale;

import me.leolin.shortcutbadger.ShortcutBadger;

public class DashboardActivity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout ll_cars;
    LinearLayout ll_spareParts;
    LinearLayout ll_marine;
    LinearLayout ll_property;
    LinearLayout anOtherView;
    boolean langFlag;
    TextView login;
    TextView signUp;
    TextView cars;
    TextView marine;
    TextView spareParts;
    TextView property;
    Toolbar toolbar;
    TextView carCounts;

    @Override
    protected void onResume() {
        super.onResume();
        if (AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.ISLOGIN).equals("true")) {
            anOtherView.setVisibility(View.GONE);
        } else {
            anOtherView.setVisibility(View.VISIBLE);
        }

        RestApis.getInstance().getCarsCount();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocaleHelper.setLocale(this, "en");
        setContentView(R.layout.activity_dashboard);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("  " + getResources().getString(R.string.dash_board));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.white_logo);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        carCounts = (TextView) findViewById(R.id.car_count);

        login = (TextView) findViewById(R.id.login);
        signUp = (TextView) findViewById(R.id.sign_up);

        anOtherView = (LinearLayout) findViewById(R.id.anotherView);
        cars = (TextView) findViewById(R.id.cars);
        marine = (TextView) findViewById(R.id.marine);
        spareParts = (TextView) findViewById(R.id.spare_parts);
        property = (TextView) findViewById(R.id.property);


        ll_cars = (LinearLayout) findViewById(R.id.ll_cars);
        ll_spareParts = (LinearLayout) findViewById(R.id.ll_spareparts);
        ll_marine = (LinearLayout) findViewById(R.id.ll_marine);
        ll_property = (LinearLayout) findViewById(R.id.ll_property);

        ll_cars.setOnClickListener(this);
        ll_spareParts.setOnClickListener(this);
        ll_marine.setOnClickListener(this);
        ll_property.setOnClickListener(this);


//        ApiClient.getClient().create(ApiInterface.class).apiCarLiveAuctionsCount().enqueue(new Callback<CountModel>() {
//            @Override
//            public void onResponse(Call<CountModel> call, Response<CountModel> response) {
//                if (response.isSuccessful()) {
//                    AppModel.getInstance().countCars = response.body().getCount_cars();
//                    carCounts.setText("" + AppModel.getInstance().countCars);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CountModel> call, Throwable t) {
//
//            }
//        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboard, menu);
        menu.getItem(0).setTitle(getString(R.string.action_language));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_language) {
            if (!langFlag) {
                langFlag = true;
                //LocaleHelper.setLocale(this, "ar-AR");
                Locale locale = new Locale("ar");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getResources().updateConfiguration(config, getResources().getDisplayMetrics());

            } else {
                langFlag = false;
                // LocaleHelper.setLocale(this, "en");
                Locale locale = new Locale("en");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getResources().updateConfiguration(config, getResources().getDisplayMetrics());
            }

            item.setTitle(this.getResources().getString(R.string.action_language));
            login.setText(this.getResources().getString(R.string.login));
            signUp.setText(this.getResources().getString(R.string.signup));
            cars.setText(this.getResources().getString(R.string.cars));
            marine.setText(this.getResources().getString(R.string.marine));
            spareParts.setText(this.getResources().getString(R.string.spare_parts));
            property.setText(this.getResources().getString(R.string.property));
            toolbar.setTitle(this.getResources().getString(R.string.dash_board));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_cars:
                if (carCounts.getText().toString().equals("0") && false) {
                    new AlertDialog.Builder(this)
                            .setMessage("Sorry , No ll_cars available for auction")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            }).show();
                } else {
                    startActivity(new Intent(this, CarsActivity.class));
                }
                break;
            case R.id.ll_spareparts:
                comingSoonPage(getString(R.string.spare_parts));
                break;
            case R.id.ll_marine:
                comingSoonPage(getString(R.string.marine));
                break;
            case R.id.ll_property:
                comingSoonPage(getString(R.string.property));
                break;
        }
    }

    public void login(View view) {
        startActivity(new Intent(this, LoginActivity.class));
    }

    public void signUp(View view) {
        startActivity(new Intent(this, RegisterActivity.class));
    }


    public void comingSoonPage(String title) {
        Intent intent = new Intent(this, PagesActivity.class);
        intent.putExtra("link", "http://www.gulfintlauction.com/coming_soon_mobile/" + getResources().getConfiguration().locale);
        intent.putExtra("title", title);
        startActivity(intent);
    }


    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(CountModel cm) {
        AppModel.getInstance().countCars = cm.getCount_cars();
        carCounts.setText("" + AppModel.getInstance().countCars);
        boolean success = ShortcutBadger.applyCount(getApplicationContext(), AppModel.getInstance().countCars);
//        startService(
//                new Intent(DashboardActivity.this, BadgeIntentService.class).putExtra("badgeCount", AppModel.getInstance().countCars)
//        );

    }
}
